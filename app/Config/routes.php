<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different urls to chosen controllers and their actions (functions).
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
	Router::connect('/', array('controller' => 'pages', 'action' => 'home'));

/**
 * About Us page
 */
	Router::connect('/nosotros', array('controller' => 'pages', 'action' => 'about_us'));

/**
 * Contact page
 */
	Router::connect('/contacto', array('controller' => 'pages', 'action' => 'contact'));

/**
* Rents page
*/
	Router::connect('/rentas', array('controller' => 'rents', 'action' => 'index'));
	Router::connect('/rentas/ver/*', array('controller' => 'rents', 'action' => 'view'));
	Router::connect('/rentas/busqueda', array('controller' => 'rents', 'action' => 'search'));

/**
* Sales page
*/
	Router::connect('/ventas', array('controller' => 'sales', 'action' => 'index'));
	Router::connect('/ventas/ver/*', array('controller' => 'sales', 'action' => 'view'));
	Router::connect('/ventas/busqueda', array('controller' => 'sales', 'action' => 'search'));

/**
 * Suscription ajax
 */
	Router::connect('/suscribir', array('controller' => 'subscribers', 'action' => 'add'));

/**
 * Contact mail ajax
 */
	Router::connect('/contacto', array('controller' => 'pages', 'action' => 'contact'));


/**
 * Admin login
 */
	Router::connect('/admin', array('controller' => 'users', 'action' => 'login'));
	Router::connect('/logout', array('controller' => 'users', 'action' => 'logout'));
	Router::connect('/admin/inicio', array('controller' => 'pages', 'action' => 'home', 'admin' => true));

/**
 * Admin Rents
 */
	Router::connect('/admin/rentas', array('controller' => 'rents', 'action' => 'index', 'admin' => true));
	Router::connect('/admin/rentas/nueva', array('controller' => 'rents', 'action' => 'add', 'admin' => true));
	Router::connect('/admin/rentas/ver/*', array('controller' => 'rents', 'action' => 'view', 'admin' => true));
	Router::connect('/admin/rentas/editar/*', array('controller' => 'rents', 'action' => 'edit', 'admin' => true));
	Router::connect('/admin/rentas/marcar-visible/*', array('controller' => 'rents', 'action' => 'toggle_active', 'admin' => true));
	Router::connect('/admin/rentas/marcar-rentada/*', array('controller' => 'rents', 'action' => 'toggle_rented', 'admin' => true));
	Router::connect('/admin/rentas/marcar-oferta/*', array('controller' => 'rents', 'action' => 'toggle_best_offer', 'admin' => true));

/**
 * Admin Sales
 */
	Router::connect('/admin/ventas', array('controller' => 'sales', 'action' => 'index', 'admin' => true));
	Router::connect('/admin/ventas/nueva', array('controller' => 'sales', 'action' => 'add', 'admin' => true));
	Router::connect('/admin/ventas/ver/*', array('controller' => 'sales', 'action' => 'view', 'admin' => true));
	Router::connect('/admin/ventas/editar/*', array('controller' => 'sales', 'action' => 'edit', 'admin' => true));
	Router::connect('/admin/ventas/marcar-vendida/*', array('controller' => 'sales', 'action' => 'toggle_sold', 'admin' => true));
	Router::connect('/admin/ventas/marcar-oferta/*', array('controller' => 'sales', 'action' => 'toggle_best_offer', 'admin' => true));

/**
 * Admin Main Images
 */
	Router::connect('/admin/imagenes-principales', array('controller' => 'main_images', 'action' => 'index', 'admin' => true));
	Router::connect('/admin/imagenes-principales/nueva', array('controller' => 'main_images', 'action' => 'add', 'admin' => true));
	Router::connect('/admin/imagenes-principales/editar/*', array('controller' => 'main_images', 'action' => 'edit', 'admin' => true));

/**
 * Admin Subscribers and Newsletter
 */
	Router::connect('/admin/suscriptores-y-boletin', array('controller' => 'subscribers', 'action' => 'index', 'admin' => true));
	Router::connect('/admin/suscriptores-y-boletin/cambiar-suscripcion/*', array('controller' => 'subscribers', 'action' => 'toggle_active', 'admin' => true));
	Router::connect('/admin/suscriptores-y-boletin/eliminar/*', array('controller' => 'subscribers', 'action' => 'delete', 'admin' => true));
	Router::connect('/admin/suscriptores-y-boletin/enviar-boletin', array('controller' => 'subscribers', 'action' => 'send_newsletter', 'admin' => true));

/**
 * Admin Galleries
 */
	Router::connect('/admin/galerias/agregar/*', array('controller' => 'default_images', 'action' => 'add', 'admin' => true));
	Router::connect('/admin/galerias/editar/*', array('controller' => 'default_images', 'action' => 'edit', 'admin' => true));

/**
 * Admin Users
 */
	Router::connect('/admin/usuarios', array('controller' => 'users', 'action' => 'index', 'admin' => true));
	Router::connect('/admin/usuarios/nuevo', array('controller' => 'users', 'action' => 'add', 'admin' => true));
	Router::connect('/admin/usuarios/ver/*', array('controller' => 'users', 'action' => 'view', 'admin' => true));
	Router::connect('/admin/usuarios/editar/*', array('controller' => 'users', 'action' => 'edit', 'admin' => true));

/**
 * Admin Configuration
 */
	Router::connect('/admin/configuracion', array('controller' => 'configurations', 'action' => 'index', 'admin' => true));
	Router::connect('/admin/configuracion/nueva', array('controller' => 'configurations', 'action' => 'add', 'admin' => true));
	Router::connect('/admin/configuracion/editar/*', array('controller' => 'configurations', 'action' => 'edit', 'admin' => true));

/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';

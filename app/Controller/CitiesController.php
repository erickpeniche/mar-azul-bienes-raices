<?php
App::uses('AppController', 'Controller');
/**
 * Cities Controller
 *
 * @property City $City
 * @property PaginatorComponent $Paginator
 */
class CitiesController extends AppController {
	/**
	 * getCities method
	 *
	 * @return void
	 */
		public function getCities($model = null, $stateId = null, $cityId = null) {
			$this->layout = 'ajax';
			$cities = $this->City->find('list', array(
				'fields' => array('City.id', 'City.name'),
				'conditions' => array('City.state_id' => $stateId)
			));

			$this->set(compact('cities', 'cityId', 'model'));
		}
}

<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController {

/**
 * Controller name
 *
 * @var string
 */
	public $name = 'Pages';

/**
 * This controller does not use a model
 *
 * @var array
 */
	public $uses = array();

	public function home() {
		$viewTitle = 'Inicio';
		//Clase Current al menu de navegación
		$this->set('home', 'current');

		$this->loadModel('MainImage');
		$this->loadModel('Sale');
		$this->loadModel('Rent');

		$mainImages = $this->MainImage->find('all', array('fields'=>array('MainImage.image')));

		// Rents
		$recentRents = $this->Rent->find('all', array('conditions' => array('Rent.active'=>1), 'order' => array('Rent.created DESC'), 'limit'=>12));
		$bestRents = $this->Rent->find('all', array('conditions' => array('Rent.active'=>1, 'AND'=>array('Rent.best_offer'=>1)), 'order' => array('Rent.created DESC'), 'limit'=>12));

		// Sales
		$recentSales = $this->Sale->find('all', array('conditions' => array('Sale.active'=>1), 'order' => array('Sale.created DESC'), 'limit'=>12));
		$bestSales = $this->Sale->find('all', array('conditions' => array('Sale.active'=>1, 'AND'=>array('Sale.best_offer'=>1)), 'order' => array('Sale.created DESC'), 'limit'=>12));
		$onSaleSales = $this->Sale->find('all', array('conditions' => array('Sale.active'=>1, 'AND'=>array('Sale.discount_available'=>1)), 'order' => array('Sale.created DESC'), 'limit'=>12));

		$this->set(compact('viewTitle', 'mainImages', 'recentRents', 'bestRents', 'recentSales', 'bestSales', 'onSaleSales'));
	}

	public function about_us (){
		$viewTitle = 'Nosotros';
		//Clase Current al menu de navegación
		$this->set('about_us', 'current');

		$this->loadModel('Sale');
		$this->loadModel('Rent');

		$bestRents = $this->Rent->find('all', array('conditions' => array('Rent.active'=>1, 'AND'=>array('Rent.best_offer'=>1)), 'order' => array('Rent.created DESC'), 'limit'=>2));
		$bestSale = $this->Sale->find('first', array('conditions' => array('Sale.active'=>1, 'AND'=>array('Sale.best_offer'=>1)), 'order' => array('Sale.created DESC')));
		$this->set(compact('viewTitle', 'bestRents', 'bestSale'));
	}

	public function contact (){
		$viewTitle = 'Contacto';
		//Clase Current al menu de navegación
		$this->set('contact', 'current');
		$confs = Configure::read('App.configurations');
		if($this->request->is('ajax')){
			$this->layout = 'ajax';
			Configure::write('debug', 0);

			$Email = new CakeEmail('marAzul');
			$Email->helpers(array('Html', 'Text'));
			$Email->template('contact', 'mar_azul');
			$Email->emailFormat('html');
			$Email->viewVars(array(
				'contactName' => $this->request->data['name'],
				'contactMail' => $this->request->data['email'],
				'contactPhone' => $this->request->data['phone'],
				'contactMessage' => $this->request->data['message'],
				'confs' => $confs
				)
			);
			$Email->to($confs['contact-email']);
			$Email->subject('Contacto '.$confs['website-title']);
			$Email->send();
		}
		$this->set(compact('confs', 'viewTitle'));
	}

	public function admin_home() {
		$this->layout = 'admin/index';
		$viewTitle = 'Panel de Administración';
		$viewSubTitle = Configure::read('App.configurations.website-title');

		$this->loadModel('User');
		$this->loadModel('Rent');
		$this->loadModel('Sale');
		$this->loadModel('MainImage');
		$this->loadModel('Subscriber');

		$totalUsers = $this->User->find('count');
		$totalRents = $this->Rent->find('count');
		$totalSales = $this->Sale->find('count');
		$totalMainImages = $this->MainImage->find('count');
		$totalSubscribers = $this->Subscriber->find('count');

		$isSuperUser = AppController::isSuperUser($this->Session->read('Auth.User'));

		$this->set(compact('totalUsers', 'totalRents', 'totalSales', 'totalMainImages', 'isSuperUser', 'viewTitle', 'viewSubTitle', 'totalSubscribers'));
	}

/**
 * Displays a view
 *
 * @param mixed What page to display
 * @return void
 */
	public function display() {
		$path = func_get_args();

		$count = count($path);
		if (!$count) {
			$this->redirect('/');
		}
		$page = $subpage = $title_for_layout = null;

		if (!empty($path[0])) {
			$page = $path[0];
		}
		if (!empty($path[1])) {
			$subpage = $path[1];
		}
		if (!empty($path[$count - 1])) {
			$title_for_layout = Inflector::humanize($path[$count - 1]);
		}
		$this->set(compact('page', 'subpage', 'title_for_layout'));
		$this->render(implode('/', $path));
	}
}

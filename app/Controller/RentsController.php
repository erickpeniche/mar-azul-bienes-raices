<?php
App::uses('AppController', 'Controller');
/**
 * Rents Controller
 *
 * @property Rent $Rent
 * @property PaginatorComponent $Paginator
 */
class RentsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->layout = 'admin/index';
		$viewTitle = 'Rentas';
		$isSuperUser = AppController::isSuperUser($this->Session->read('Auth.User'));

		$this->Rent->recursive = 0;
		$this->set('rents', $this->Paginator->paginate());
		$this->set(compact('isSuperUser', 'viewTitle'));
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->layout = 'admin/index';
		$viewTitle = 'Rentas';
		$isSuperUser = AppController::isSuperUser($this->Session->read('Auth.User'));
		if (!$this->Rent->exists($id)) {
			throw new NotFoundException(__('Invalid rent'));
		}
		$options = array('conditions' => array('Rent.' . $this->Rent->primaryKey => $id));
		$this->set('rent', $this->Rent->find('first', $options));
		$this->set(compact('isSuperUser', 'viewTitle'));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		$this->layout = 'admin/index';
		$isSuperUser = $this->isSuperUser($this->Session->read('Auth.User'));
		if(!$isSuperUser){
			$this->Session->setFlash('No tienes los permisos para acceder a esta sección.', 'admin/custom_flash_security');
			$this->redirect(array('controller' => 'pages', 'action' => 'home', 'admin' => true));
		}
		$viewTitle = 'Nueva';
		$viewSubTitle = 'Renta';

		$states = $this->Rent->State->find('list', array(
			'fields' => array('State.id', 'State.name')
		));

		if ($this->request->is('post')) {
			$this->Rent->create();
			if ($this->Rent->save($this->request->data)) {
				$this->Session->setFlash('La propiedad en renta ha sido creada.', 'admin/custom_flash_success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('La propiedad en renta no se pudo crear.', 'admin/custom_flash_error');
			}
		}
		$this->set(compact('isSuperUser', 'viewTitle', 'viewSubTitle', 'states'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->layout = 'admin/index';
		if (!$this->Rent->exists($id)) {
			throw new NotFoundException(__('Invalid rent'));
		}else{
			$isSuperUser = $this->isSuperUser($this->Session->read('Auth.User'));
			$viewTitle = 'Editar';
			$viewSubTitle = 'Renta';
		}

		$states = $this->Rent->State->find('list', array(
			'fields' => array('State.id', 'State.name')
		));

		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Rent->save($this->request->data)) {
				$this->Session->setFlash('La propiedad en renta ha sido editada', 'admin/custom_flash_success');
				$this->redirect(array('action' => 'view', $this->request->data['Rent']['id']));
			} else {
				$this->Session->setFlash('No se pudo editar la propiedad en renta.', 'admin/custom_flash_error');
				$options = array('conditions' => array('Rent.' . $this->Rent->primaryKey => $id));
				$this->request->data = $this->Rent->find('first', $options);
			}
		} else {
			$options = array('conditions' => array('Rent.' . $this->Rent->primaryKey => $id));
			$this->request->data = $this->Rent->find('first', $options);
		}
		$this->set(compact('isSuperUser', 'viewTitle', 'viewSubTitle', 'states'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Rent->id = $id;
		if (!$this->Rent->exists()) {
			throw new NotFoundException(__('Invalid rent'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Rent->delete()) {
			$this->Session->setFlash('La propiedad en renta ha sido eliminada.', 'admin/custom_flash_alert');
			return $this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash('No se pudo eliminar La propiedad en renta.', 'admin/custom_flash_error');
		return $this->redirect(array('action' => 'index'));
	}

	public function admin_toggle_active($id = null) {
		$this->Rent->id = $id;
		if (!$this->Rent->exists()) {
			throw new NotFoundException(__('Invalid rent'));
		}
		$options = array('conditions' => array('Rent.' . $this->Rent->primaryKey => $id));
		$rent = $this->Rent->find('first', $options);

		if ($rent['Rent']['active']) {
			$active = 0;
			$status = 'OCULTA';
		} else {
			$active = 1;
			$status = 'VISIBLE';
		}

		$this->Rent->set('active', $active);
		if ($this->Rent->save()) {
			$this->Session->setFlash('La propiedad se ha marcado como <strong>'.$status.'<strong>.', 'admin/custom_flash_success');
			return $this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash('No se pudo editar la propiedad en renta.', 'admin/custom_flash_error');
		return $this->redirect(array('action' => 'index'));
	}

	public function admin_toggle_rented($id = null) {
		$this->Rent->id = $id;
		if (!$this->Rent->exists()) {
			throw new NotFoundException(__('Invalid rent'));
		}
		$options = array('conditions' => array('Rent.' . $this->Rent->primaryKey => $id));
		$rent = $this->Rent->find('first', $options);

		if ($rent['Rent']['rented']) {
			$rented = 0;
			$status = 'SIN RENTAR';
		} else {
			$rented = 1;
			$status = 'RENTADA';
		}

		$this->Rent->set('rented', $rented);
		if ($this->Rent->save()) {
			$this->Session->setFlash('La propiedad se ha marcado como <strong>'.$status.'<strong>.', 'admin/custom_flash_success');
			return $this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash('No se pudo editar la propiedad en renta.', 'admin/custom_flash_error');
		return $this->redirect(array('action' => 'index'));
	}

	public function admin_toggle_best_offer($id = null) {
		$this->Rent->id = $id;
		if (!$this->Rent->exists()) {
			throw new NotFoundException(__('Invalid rent'));
		}
		$options = array('conditions' => array('Rent.' . $this->Rent->primaryKey => $id));
		$rent = $this->Rent->find('first', $options);

		if ($rent['Rent']['best_offer']) {
			$bestOffer = 0;
			$status = 'NORMAL';
		} else {
			$bestOffer = 1;
			$status = 'OFERTA DESTACADA';
		}

		$this->Rent->set('best_offer', $bestOffer);
		if ($this->Rent->save()) {
			$this->Session->setFlash('La propiedad se ha marcado como <strong>'.$status.'<strong>.', 'admin/custom_flash_success');
			return $this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash('No se pudo editar la propiedad en renta.', 'admin/custom_flash_error');
		return $this->redirect(array('action' => 'index'));
	}

	public function index(){
		$viewTitle = 'Rentas';
		$this->set('rentsMenu', 'current');

		$this->Rent->recursive = 0;
		$this->set('rents', $this->Paginator->paginate());
		$this->set(compact('viewTitle'));
	}

	public function view($id=null){
		$viewTitle = 'Rentas';
		$this->set('rentsMenu', 'current');

		if (!$this->Rent->exists($id)) {
			throw new NotFoundException(__('Invalid rent'));
		}
		$options = array('conditions' => array('Rent.' . $this->Rent->primaryKey => $id));
		$this->set('rent', $this->Rent->find('first', $options));
		$this->set(compact('viewTitle'));
	}

	public function search(){
		if ($this->request->is('post')) {
			$viewTitle = 'Rentas';
			$this->set('rentsMenu', 'current');

			$contain = array('City', 'State');

			$emptyFields = array(
				'stateCity' => empty($this->data['Rent']['state_city']),
				'type' => $this->data['Rent']['type'] == '0',
				'minPrice' => empty($this->data['Rent']['min_price']),
				'maxPrice' => empty($this->data['Rent']['max_price']),
				'floors' => $this->data['Rent']['floors'] == '0',
				'bedrooms' => $this->data['Rent']['bedrooms'] == '0',
				'bathrooms' => $this->data['Rent']['bathrooms'] == '0'
			);

			switch ($this->data['Rent']['type']) {
				case '0':
					$type = null;
					break;
				case '1':
					$type = 'Casa';
					break;
				case '2':
					$type = 'Departamento';
					break;
			}

			$subconditionFloors = '';
			switch ($this->data['Rent']['floors']) {
				case '0':
					$floors = null;
					break;
				case '1':
					$floors = 1;
					break;
				case '2':
					$floors = 2;
					break;
				case '3':
					$floors = 3;
					$subconditionFloors = ' >=';
					break;
			}

			$subconditionBedrooms = '';
			switch ($this->data['Rent']['bedrooms']) {
				case '0':
					$bedrooms = null;
					break;
				case '1':
					$bedrooms = 1;
					break;
				case '2':
					$bedrooms = 2;
					break;
				case '3':
					$bedrooms = 3;
					break;
				case '4':
					$bedrooms = 4;
					break;
				case '5':
					$bedrooms = 5;
					$subconditionBedrooms = ' >=';
					break;
			}

			$subconditionBathrooms = '';
			switch ($this->data['Rent']['bathrooms']) {
				case '0':
					$bathrooms = null;
					break;
				case '1':
					$bathrooms = 1;
					break;
				case '2':
					$bathrooms = 2;
					break;
				case '3':
					$bathrooms = 3;
					break;
				case '4':
					$bathrooms = 4;
					break;
				case '5':
					$bathrooms = 5;
					$subconditionBathrooms = ' >=';
					break;
			}

			$conditions = array(
				'OR' => array(
					'State.name LIKE' => !$emptyFields['stateCity'] ? '%'.$this->data['Rent']['state_city'].'%' : '',
					'City.name LIKE' => !$emptyFields['stateCity'] ? '%'.$this->data['Rent']['state_city'].'%' : ''
				),
				'AND' => array(
					'Rent.type' => $type,
					'Rent.cost BETWEEN ? AND ?' => array($this->data['Rent']['min_price'], $this->data['Rent']['max_price']),
					'Rent.floors'.$subconditionFloors => $floors,
					'Rent.bedrooms'.$subconditionBedrooms => $bedrooms,
					'Rent.bathrooms'.$subconditionBathrooms => $bathrooms
				)
			);

			if ($emptyFields['stateCity']) { unset($conditions['OR']); };
			if ($emptyFields['type']) { unset($conditions['AND']['Rent.type']); };
			if ($emptyFields['minPrice'] && $emptyFields['maxPrice']) { unset($conditions['AND']['Rent.cost BETWEEN ? AND ?']); };
			if ($emptyFields['floors']) { unset($conditions['AND']['Rent.floors']); };
			if ($emptyFields['bedrooms']) { unset($conditions['AND']['Rent.bedrooms']); };
			if ($emptyFields['bathrooms']) { unset($conditions['AND']['Rent.bathrooms']); };

			$rents = $this->Rent->find('all', array('conditions'=>$conditions, 'contain'=>$contain));
			$this->set(compact('viewTitle', 'rents'));
		}else{
			$this->redirect(array('action'=>'index'));
		}
	}
}
<?php
App::uses('AppController', 'Controller');
/**
 * Sales Controller
 *
 * @property Sale $Sale
 * @property PaginatorComponent $Paginator
 */
class SalesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->layout = 'admin/index';
		$viewTitle = 'Ventas';
		$isSuperUser = AppController::isSuperUser($this->Session->read('Auth.User'));

		$this->Sale->recursive = 0;
		$this->set('sales', $this->Paginator->paginate());
		$this->set(compact('isSuperUser', 'viewTitle'));
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->layout = 'admin/index';
		$viewTitle = 'Ventas';
		$isSuperUser = AppController::isSuperUser($this->Session->read('Auth.User'));
		if (!$this->Sale->exists($id)) {
			throw new NotFoundException(__('Invalid sale'));
		}
		$options = array('conditions' => array('Sale.' . $this->Sale->primaryKey => $id));
		$this->set('sale', $this->Sale->find('first', $options));
		$this->set(compact('isSuperUser', 'viewTitle'));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		$this->layout = 'admin/index';
		$isSuperUser = $this->isSuperUser($this->Session->read('Auth.User'));
		if(!$isSuperUser){
			$this->Session->setFlash('No tienes los permisos para acceder a esta sección.', 'admin/custom_flash_security');
			$this->redirect(array('controller' => 'pages', 'action' => 'home', 'admin' => true));
		}
		$viewTitle = 'Nueva';
		$viewSubTitle = 'Venta';

		$states = $this->Sale->State->find('list', array(
			'fields' => array('State.id', 'State.name')
		));

		if ($this->request->is('post')) {
			$this->Sale->create();
			if ($this->Sale->save($this->request->data)) {
				$this->Session->setFlash('La propiedad en venta ha sido creada.', 'admin/custom_flash_success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('La propiedad en venta no se pudo crear.', 'admin/custom_flash_error');
			}
		}
		$this->set(compact('isSuperUser', 'viewTitle', 'viewSubTitle', 'states'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->layout = 'admin/index';
		if (!$this->Sale->exists($id)) {
			throw new NotFoundException(__('Invalid sale'));
		}else{
			$isSuperUser = $this->isSuperUser($this->Session->read('Auth.User'));
			$viewTitle = 'Editar';
			$viewSubTitle = 'Venta';
		}

		$states = $this->Sale->State->find('list', array(
			'fields' => array('State.id', 'State.name')
		));

		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Sale->save($this->request->data)) {
				$this->Session->setFlash('La propiedad en venta ha sido editada', 'admin/custom_flash_success');
				$this->redirect(array('action' => 'view', $this->request->data['Sale']['id']));
			} else {
				$this->Session->setFlash('No se pudo editar la propiedad en venta.', 'admin/custom_flash_error');
				$options = array('conditions' => array('Sale.' . $this->Sale->primaryKey => $id));
				$this->request->data = $this->Sale->find('first', $options);
			}
		} else {
			$options = array('conditions' => array('Sale.' . $this->Sale->primaryKey => $id));
			$this->request->data = $this->Sale->find('first', $options);
		}
		$this->set(compact('isSuperUser', 'viewTitle', 'viewSubTitle', 'states'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Sale->id = $id;
		if (!$this->Sale->exists()) {
			throw new NotFoundException(__('Invalid sale'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Sale->delete()) {
			$this->Session->setFlash('La propiedad en venta ha sido eliminada.', 'admin/custom_flash_alert');
			return $this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash('No se pudo eliminar La propiedad en venta.', 'admin/custom_flash_error');
		return $this->redirect(array('action' => 'index'));
	}

	public function admin_toggle_sold($id = null) {
		$this->Sale->id = $id;
		if (!$this->Sale->exists()) {
			throw new NotFoundException(__('Invalid sale'));
		}
		$options = array('conditions' => array('Sale.' . $this->Sale->primaryKey => $id));
		$sale = $this->Sale->find('first', $options);

		if ($sale['Sale']['active']) {
			$active = 0;
			$status = 'VENDIDA';
		} else {
			$active = 1;
			$status = 'EN VENTA';
		}

		$this->Sale->set('active', $active);
		if ($this->Sale->save()) {
			$this->Session->setFlash('La propiedad se ha marcado como <strong>'.$status.'<strong>.', 'admin/custom_flash_success');
			return $this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash('No se pudo editar la propiedad en venta.', 'admin/custom_flash_error');
		return $this->redirect(array('action' => 'index'));
	}

	public function admin_toggle_best_offer($id = null) {
		$this->Sale->id = $id;
		if (!$this->Sale->exists()) {
			throw new NotFoundException(__('Invalid sale'));
		}
		$options = array('conditions' => array('Sale.' . $this->Sale->primaryKey => $id));
		$sale = $this->Sale->find('first', $options);

		if ($sale['Sale']['best_offer']) {
			$bestOffer = 0;
			$status = 'NORMAL';
		} else {
			$bestOffer = 1;
			$status = 'OFERTA DESTACADA';
		}

		$this->Sale->set('best_offer', $bestOffer);
		if ($this->Sale->save()) {
			$this->Session->setFlash('La propiedad se ha marcado como <strong>'.$status.'<strong>.', 'admin/custom_flash_success');
			return $this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash('No se pudo editar la propiedad en venta.', 'admin/custom_flash_error');
		return $this->redirect(array('action' => 'index'));
	}

	public function index(){
		$viewTitle = 'Ventas';
		$this->set('salesMenu', 'current');

		$this->Sale->recursive = 0;
		$this->set('sales', $this->Paginator->paginate());
		$this->set(compact('viewTitle'));
	}

	public function view($id=null){
		$viewTitle = 'Ventas';
		$this->set('salesMenu', 'current');

		if (!$this->Sale->exists($id)) {
			throw new NotFoundException(__('Invalid sale'));
		}
		$options = array('conditions' => array('Sale.' . $this->Sale->primaryKey => $id));
		$this->set('sale', $this->Sale->find('first', $options));
		$this->set(compact('viewTitle'));
	}

	public function search(){
		if ($this->request->is('post')) {
			$viewTitle = 'Ventas';
			$this->set('salesMenu', 'current');

			$contain = array('City', 'State');

			$emptyFields = array(
				'stateCity' => empty($this->data['Sale']['state_city']),
				'type' => $this->data['Sale']['type'] == '0',
				'minPrice' => empty($this->data['Sale']['min_price']),
				'maxPrice' => empty($this->data['Sale']['max_price']),
				'floors' => $this->data['Sale']['floors'] == '0',
				'bedrooms' => $this->data['Sale']['bedrooms'] == '0',
				'bathrooms' => $this->data['Sale']['bathrooms'] == '0'
			);

			switch ($this->data['Sale']['type']) {
				case '0':
					$type = null;
					break;
				case '1':
					$type = 'Casa';
					break;
				case '2':
					$type = 'Departamento';
					break;
				case '3':
					$type = 'Terreno';
					break;
			}

			$subconditionFloors = '';
			switch ($this->data['Sale']['floors']) {
				case '0':
					$floors = null;
					break;
				case '1':
					$floors = 1;
					break;
				case '2':
					$floors = 2;
					break;
				case '3':
					$floors = 3;
					$subconditionFloors = ' >=';
					break;
			}

			$subconditionBedrooms = '';
			switch ($this->data['Sale']['bedrooms']) {
				case '0':
					$bedrooms = null;
					break;
				case '1':
					$bedrooms = 1;
					break;
				case '2':
					$bedrooms = 2;
					break;
				case '3':
					$bedrooms = 3;
					break;
				case '4':
					$bedrooms = 4;
					break;
				case '5':
					$bedrooms = 5;
					$subconditionBedrooms = ' >=';
					break;
			}

			$subconditionBathrooms = '';
			switch ($this->data['Sale']['bathrooms']) {
				case '0':
					$bathrooms = null;
					break;
				case '1':
					$bathrooms = 1;
					break;
				case '2':
					$bathrooms = 2;
					break;
				case '3':
					$bathrooms = 3;
					break;
				case '4':
					$bathrooms = 4;
					break;
				case '5':
					$bathrooms = 5;
					$subconditionBathrooms = ' >=';
					break;
			}

			$conditions = array(
				'OR' => array(
					'State.name LIKE' => !$emptyFields['stateCity'] ? '%'.$this->data['Sale']['state_city'].'%' : '',
					'City.name LIKE' => !$emptyFields['stateCity'] ? '%'.$this->data['Sale']['state_city'].'%' : ''
				),
				'AND' => array(
					'Sale.type' => $type,
					'Sale.cost BETWEEN ? AND ?' => array($this->data['Sale']['min_price'], $this->data['Sale']['max_price']),
					'Sale.floors'.$subconditionFloors => $floors,
					'Sale.bedrooms'.$subconditionBedrooms => $bedrooms,
					'Sale.bathrooms'.$subconditionBathrooms => $bathrooms
				)
			);

			if ($emptyFields['stateCity']) { unset($conditions['OR']); };
			if ($emptyFields['type']) { unset($conditions['AND']['Sale.type']); };
			if ($emptyFields['minPrice'] && $emptyFields['maxPrice']) { unset($conditions['AND']['Sale.cost BETWEEN ? AND ?']); };
			if ($emptyFields['floors']) { unset($conditions['AND']['Sale.floors']); };
			if ($emptyFields['bedrooms']) { unset($conditions['AND']['Sale.bedrooms']); };
			if ($emptyFields['bathrooms']) { unset($conditions['AND']['Sale.bathrooms']); };

			$sales = $this->Sale->find('all', array('conditions'=>$conditions, 'contain'=>$contain));
			$this->set(compact('viewTitle', 'sales'));
		}else{
			$this->redirect(array('action'=>'index'));
		}
	}
}

<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
/**
 * Subscribers Controller
 *
 * @property Subscriber $Subscriber
 * @property PaginatorComponent $Paginator
 */
class SubscribersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->layout = 'admin/index';
		$viewTitle = 'Suscriptores y boletín';
		$this->Subscriber->recursive = 0;
		$this->set('subscribers', $this->Paginator->paginate());
		$this->set(compact('viewTitle'));
	}

	public function admin_send_newsletter() {
		$subscribers = $this->Subscriber->find('list',
			array(
				'conditions' => array('Subscriber.active'=>1),
				'fields' => array('Subscriber.id', 'Subscriber.email')
			)
		);

		$confs = Configure::read('App.configurations');

		$this->loadModel('MainImage');
		$this->loadModel('Sale');
		$this->loadModel('Rent');

		$mainImages = $this->MainImage->find('all', array(
			'fields'=>array('MainImage.image'),
			'limit'=> 4,
			'order'=> array('MainImage.created DESC')
			)
		);

		$options = array('conditions' => array('Rent.active'=>1), 'order' => array('Rent.created DESC'), 'limit'=>3);
		$recentRents = $this->Rent->find('all', $options);

		$options = array('conditions' => array('Sale.active'=>1), 'order' => array('Sale.created DESC'), 'limit'=>3);
		$recentSales = $this->Sale->find('all', $options);

		foreach ($subscribers as $id => $mail) {
			$Email = new CakeEmail('marAzul');
			$Email->helpers(array('Html', 'Text'));
			$Email->template('newsletter', 'mar_azul');
			$Email->emailFormat('html');
			$Email->viewVars(array(
				'mainImages' => $mainImages,
				'recentRents' => $recentRents,
				'recentSales' => $recentSales,
				'confs' => $confs,
				'subscriberEmail' => $mail,
				'subscriberId' => $id
				)
			);
			$Email->to($mail);
			$Email->subject('Mar Azul Bienes Raíces - Novedades');
			$Email->send();
		}

		$this->Session->setFlash('Se ha completado el proceso de envío del boletín.', 'admin/custom_flash_success');
		$this->redirect(array('action' => 'index'));
	}

	public function admin_toggle_active($id = null) {
		$this->Subscriber->id = $id;
		if (!$this->Subscriber->exists()) {
			throw new NotFoundException(__('Invalid subscriber'));
		}
		$options = array('conditions' => array('Subscriber.' . $this->Subscriber->primaryKey => $id));
		$subscriber = $this->Subscriber->find('first', $options);

		if ($subscriber['Subscriber']['active']) {
			$active = 0;
			$status = 'dado de baja de la suscripción';
		} else {
			$active = 1;
			$status = 'reinscrito al servicio';
		}

		$this->Subscriber->set('active', $active);
		if ($this->Subscriber->save()) {
			$this->Session->setFlash('La dirección de correo electrónico se ha <strong>'.$status.'<strong>.', 'admin/custom_flash_success');
			return $this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash('No se pudo editar la propiedad en renta.', 'admin/custom_flash_error');
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Subscriber->id = $id;
		if (!$this->Subscriber->exists()) {
			throw new NotFoundException(__('Invalid subscriber'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Subscriber->delete()) {
			$this->Session->setFlash(__('Subscriber deleted'));
			return $this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Subscriber was not deleted'));
		return $this->redirect(array('action' => 'index'));
	}

	public function add(){
		if($this->request->is('ajax')){
			Configure::write('debug', 0);
			$this->layout = 'ajax';
			$this->Subscriber->create();
			$this->Subscriber->save($this->request->data);
		}else{
			$this->redirect(array('controller' => 'pages','action' => 'home'));
		}
	}
}

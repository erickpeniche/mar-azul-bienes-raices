<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 */
class UsersController extends AppController {
	public function login() {
		$this->layout = 'admin/login';

		if($this->Auth->loggedIn()) {
			$this->redirect($this->Auth->redirect());
		}

		if ($this->request->is('post')) {
			if ($this->Auth->login()) {
				$this->Session->setFlash('Bienvenido '.$this->Auth->user('first_name').' '.$this->Auth->user('last_name').'!', 'admin/custom_flash_info');
				$this->redirect($this->Auth->redirect());
			} else {
				$this->Session->setFlash(__('La combinación de usuario y contraseña no son válidas o el usuario se encuentra inactivo.'), 'admin/login_flash', array(), 'auth');
			}
		}
	}

	public function logout() {
		$this->redirect($this->Auth->logout());
	}
/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->layout = 'admin/index';
		$viewTitle = 'Usuarios';

		$isSuperUser = $this->isSuperUser($this->Session->read('Auth.User'));
		$loggedUser = $this->Session->read('Auth.User');
		if(!$isSuperUser){
			$this->Session->setFlash('No tienes los permisos para acceder a esta sección.', 'admin/custom_flash_security');
			$this->redirect(array('controller' => 'pages', 'action' => 'home', 'admin' => true));
		}
		$this->User->recursive = 0;
		$this->set('users', $this->paginate());
		$this->set(compact('isSuperUser', 'viewTitle', 'loggedUser'));
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->layout = 'admin/index';
		$loggedUser = $this->Session->read('Auth.User');
		$isSuperUser = $this->isSuperUser($this->Session->read('Auth.User'));
		if(!$isSuperUser && $this->Session->read('Auth.User.id')!=$id){
			$this->Session->setFlash('No tienes los permisos para acceder a esta sección.', 'admin/custom_flash_security');
			$this->redirect(array('controller' => 'pages', 'action' => 'home', 'admin' => true));
		}
		$viewTitle = 'Ver';
		$viewSubTitle = 'Usuario';
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->set('user', $this->User->find('first', $options));
		$this->set(compact('isSuperUser', 'viewTitle', 'viewSubTitle', 'loggedUser'));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		$this->layout = 'admin/index';
		$isSuperUser = $this->isSuperUser($this->Session->read('Auth.User'));
		if(!$isSuperUser){
			$this->Session->setFlash('No tienes los permisos para acceder a esta sección.', 'admin/custom_flash_security');
			$this->redirect(array('controller' => 'pages', 'action' => 'home', 'admin' => true));
		}
		$viewTitle = 'Nuevo';
		$viewSubTitle = 'Usuario';

		if ($this->request->is('post')) {
			$this->User->create();
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash('El usuario '.$this->request->data['User']['name']. ' ha sido creado.', 'admin/custom_flash_success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('El usuario '.$this->request->data['User']['name']. ' no se pudo crear.', 'admin/custom_flash_error');
			}
		}
		$this->set(compact('isSuperUser', 'viewTitle', 'viewSubTitle'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null, $name = null) {
		$this->layout = 'admin/index';
		$isSuperUser = $this->isSuperUser($this->Session->read('Auth.User'));
		if(!$isSuperUser && $this->Session->read('Auth.User.id')!=$id){
			$this->Session->setFlash('No tienes los permisos para acceder a esta sección.', 'admin/custom_flash_security');
			$this->redirect(array('controller' => 'pages', 'action' => 'home', 'admin' => true));
		}
		$viewTitle = 'Editar';
		$viewSubTitle = 'Usuario';

		$loggedUser = $this->Session->read('Auth.User');
		$ownAccount = $id == $loggedUser['id'] ? true : false;

		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}

		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$user = $this->User->find('first', $options);

		if($this->request->is('ajax')){
			Configure::write('debug', 0);
			$this->layout = 'ajax';
			$this->User->save($this->request->data);
		}elseif($this->request->is('post') || $this->request->is('put')) {
			if(!empty($this->data['User']['image'])){
				if ($this->User->save($this->request->data)) {
					$this->Session->setFlash('La imagen del usuario ha sido editada.', 'admin/custom_flash_success');
				} else {
					$this->Session->setFlash('La imagen del usuario no se pudo editar.', 'admin/custom_flash_error');
				}
				$this->redirect(array('action' => 'edit', $id, $name));
			}else{
				if ($this->User->save($this->request->data)) {
					$this->Session->setFlash('La contraseña ha sido editada.', 'admin/custom_flash_success');
					$this->redirect(array('action' => 'logout', 'admin'=>false));
				} else {
					$this->Session->setFlash('La contraseña no se pudo editar.', 'admin/custom_flash_error');
				}
			}
		}
		$this->request->data = $user;
		$this->set(compact('user', 'isSuperUser', 'viewTitle', 'viewSubTitle', 'ownAccount'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$isSuperUser = $this->isSuperUser($this->Session->read('Auth.User'));
		if(!$isSuperUser){
			$this->Session->setFlash('No tienes los permisos para acceder a esta sección.', 'admin/custom_flash_security');
			$this->redirect(array('controller' => 'pages', 'action' => 'home', 'admin' => true));
		}
		if ($this->Session->read('Auth.User.id')==$id) {
			$this->Session->setFlash('No puedes realizar esta accion. (No te eliminarías a ti mismo o si?)', 'admin/custom_flash_error');
			$this->redirect(array('action' => 'index', 'admin' => true));
		}
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->User->delete()) {
			$this->Session->setFlash('Usuario eliminado', 'admin/custom_flash_alert');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash('No se pudo eliminar al usuario.', 'admin/custom_flash_error');
		$this->redirect(array('action' => 'index'));
	}

	public function isSuperUser($user) {
		if (isset($user['role']) && $user['role'] === 'Super User') {
			return true;
		}
		return false;
	}
}

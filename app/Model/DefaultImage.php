<?php
App::uses('AppModel', 'Model');
/**
 * DefaultImage Model
 *
 * @property News $News
 * @property Plant $Plant
 * @property Technology $Technology
 * @property EducationalPlan $EducationalPlan
 * @property EducationalProgram $EducationalProgram
 * @property Project $Project
 * @property ImprovementPlan $ImprovementPlan
 * @property Event $Event
 */
class DefaultImage extends AppModel {

	public $validate = array(
		'file' => array(
			'checksize' => array(
				'rule' => array('checkSize',true),
				'message' => false
			),
			'checktype' =>array(
				'rule' => array('checkType',true),
				'message' => false
			),
			'checkupload' =>array(
				'rule' => array('checkUpload', true),
				'message' => false
			),
		)
	);

	function checkUpload($data, $required = false){
		$data = array_shift($data);
		if(!$required && $data['error'] == 4){
			return true;
		}
		if($required && $data['error'] != 0){
			return false;
		}
		if($data['size'] == 0){
			return false;
		}
		return true;
	}

	function checkType($data, $required = false){
		$data = array_shift($data);
		if(!$required && $data['error'] == 4){
			return true;
		}
		$allowedExtensions = array('jpg', 'JPG', 'jpeg', 'JPEG', 'png', 'PNG', 'bmp', 'BMP');
		$extension = pathinfo($data['name'], PATHINFO_EXTENSION);

		if(!in_array($extension, $allowedExtensions)){
			return false;
		}
		return true;
	}

	function checkSize($data, $required = false){
		$data = array_shift($data);
		if(!$required && $data['error'] == 4){
			return true;
		}

		if($data['size'] == 0 ||$data['size']/1024 > 20480){
			return false;
		}
		return true;
	}
/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Sale' => array(
			'className' => 'Sale',
			'foreignKey' => 'foreign_key',
			'conditions' => array('model'=>'Sale'),
			'fields' => '',
			'order' => ''
		),
		'Rent' => array(
			'className' => 'Rent',
			'foreignKey' => 'foreign_key',
			'conditions' => array('model'=>'Rent'),
			'fields' => '',
			'order' => ''
		)
	);

	public function beforeDelete() {
		$image = $this->find('first', array('conditions' => array('DefaultImage.id' => $this->id)));

		$model = $image['DefaultImage']['model'];
		App::import('model', $model);
		$this->{$model} = new $model();

		if (!empty($image['DefaultImage']['file'])) {
			if (isset($this->{$model}->images)) {
				$setup = $this->{$model}->images;
				$imageDirectory = WWW_ROOT . 'img' . DS . 'images' . DS;
				foreach($setup as $key => $set) {
					unlink($imageDirectory . DS . $key . DS . $key . '_' . $image['DefaultImage']['file']);
				}
			} else {
				$imageDirectory = WWW_ROOT . 'img' . DS . 'images' . DS;
				unlink($imageDirectory . 'rents' . DS . $image['DefaultImage']['file']);
				unlink($imageDirectory . 'sales' . DS . $image['DefaultImage']['file']);
			}
		}
		return true;
	}
}
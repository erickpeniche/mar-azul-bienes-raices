<?php
App::uses('AppModel', 'Model');
/**
 * MainImage Model
 *
 */
class MainImage extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'image' => array(
			'checksizeedit' => array(
				'rule' => array('checkSize',false),
				'message' => false,
				'on' => 'update'
				),
			'checktypeedit' =>array(
				'rule' => array('checkType',false),
				'message' => false,
				'on' => 'update'
				),
			'checkuploadedit' =>array(
				'rule' => array('checkUpload', false),
				'message' => false,
				'on' => 'update'
				),
			'checksize' => array(
				'rule' => array('checkSize',true),
				'message' => false,
				'on' => 'create'
				),
			'checktype' =>array(
				'rule' => array('checkType',true),
				'message' => false,
				'on' => 'create'
				),
			'checkupload' =>array(
				'rule' => array('checkUpload', true),
				'message' => false,
				'on' => 'create'
				),
		)
	);

	function checkUpload($data, $required = false){
		$data = array_shift($data);
		if(!$required && $data['error'] == 4){
			return true;
		}
		if($required && $data['error'] !== 0){
			return false;
		}
		if($data['size'] == 0){
			return false;
		}
		return true;
	}

	function checkType($data, $required = false){
		$data = array_shift($data);
		if(!$required && $data['error'] == 4){
			return true;
		}
		$allowedExtensions = array('jpg', 'JPG', 'jpeg', 'JPEG', 'png', 'PNG', 'bmp', 'BMP');
		$extension = pathinfo($data['name'], PATHINFO_EXTENSION);

		if(!in_array($extension, $allowedExtensions)){
			return false;
		}
		return true;
	}

	function checkSize($data, $required = false){
		$data = array_shift($data);
		if(!$required && $data['error'] == 4){
			return true;
		}

		if($data['size'] == 0 ||$data['size']/1024 > 20480){
			return false;
		}
		return true;
	}

	function beforeSave() {
		$return = false;
		$image = null;
		$erase = false;
		$mainImage = null;

		if (!empty($this->id)) {
			$mainImage = $this->find('first', array('conditions' => array('MainImage.id' => $this->data['MainImage']['id'])));
			$image = $mainImage['MainImage']['image'];
		}

		//File upload
		$file = isset($this->data['MainImage']['image']) ? $this->data['MainImage']['image'] : null;
		$fileDir = WWW_ROOT . 'img' . DS . 'images';
		$fileName = '';
		$fileExtension = '';

		//A file was uploaded
		if (!empty($file) && !empty($file['tmp_name']) && $file['error'] != 4) {
			//There is no error, the file was uploaded with success
			if ($file['error'] == 0) {

				//Tells whether the file was uploaded via HTTP POST
				if (is_uploaded_file($file['tmp_name'])) {
					App::import('Vendor', 'Bitmap');
					App::import('Vendor', 'BitmapException');

					if (Bitmap::isImage($file['tmp_name'])) {

						try {
							$fileName = md5($file['tmp_name'] . (rand() * 100000));
							$imageSize = getimagesize($file['tmp_name']);
							$width = $imageSize[0];
							$height = $imageSize[1];

							$set['height'] = 369;
							$set['width'] = 940;
							$set['align'] = 'center';
							$set['valign'] = 'middle';

							$home = new Bitmap($file['tmp_name']);
							$home->open();
							$home->resizeAndCrop2($imageSize, $set);
							$home->save($fileDir, 'main_home_' . $fileName);
							$home->dispose();

							$set['height'] = 150;
							$set['width'] = 200;
							$set['align'] = 'center';
							$set['valign'] = 'middle';

							$edt = new Bitmap($file['tmp_name']);
							$edt->open();
							$edt->resizeAndCrop2($imageSize, $set);
							$edt->save($fileDir, 'main_edt_' . $fileName);
							$edt->dispose();

							$img = new Bitmap($file['tmp_name']);
							$img->open();
							$img->save($fileDir, 'main_img_' . $fileName);
							$img->dispose();
							$fileExtension = $img->getExtension();

							$this->data['MainImage']['size'] = $file['size'];
							$this->data['MainImage']['image'] = $fileName . '.' .  $fileExtension;
							$return = true;
							$erase = true;
						} catch (BitmapException $e) {
							$this->invalidate('image', 'bitmapError');
						}
					} else {
						$this->invalidate('image', 'unsupportedType');
					}
				} else {
					$this->invalidate('image', 'maliciousUpload');
				}
			} else {
				//File upload error. See http://us3.php.net/manual/en/features.file-upload.errors.php
				$this->invalidate('image', 'uploadError');
			}
		} else {
			if (isset($this->data['MainImage']['erase']) && $this->data['MainImage']['erase']) {
				$this->data['MainImage']['image'] = null;
			} else {
				unset($this->data['MainImage']['image']);
			}
			$return = true;
		}

		// REMOVE PREVIOUS LOGO
		if (!empty($image) && ((isset($this->data['MainImage']['erase']) && $this->data['MainImage']['erase']) || $erase)) {
			unlink($fileDir . DS . 'main_home_' . $image);
			unlink($fileDir . DS . 'main_img_' . $image);
			unlink($fileDir . DS . 'main_edt_' . $image);
		}

		return $return;
	}

	function beforeDelete() {
		$mainImage = $this->find('first', array('conditions' => array('MainImage.id' => $this->id), 'contain' => false));

		if (!empty($mainImage['MainImage']['image'])) {
			$imageDirectory = WWW_ROOT . 'img' . DS . 'images' . DS;
			unlink($imageDirectory . 'main_img_' . $mainImage['MainImage']['image']);
			unlink($imageDirectory . 'main_home_' . $mainImage['MainImage']['image']);
			unlink($imageDirectory . 'main_edt_' . $mainImage['MainImage']['image']);
		}

		return true;
	}
}

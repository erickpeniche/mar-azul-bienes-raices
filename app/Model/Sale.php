<?php
App::uses('AppModel', 'Model');
/**
 * Sale Model
 *
 * @property DefaultImage $DefaultImage
 */
class Sale extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'description' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => false
			),
		),
		'state_id' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => false
			),
		),
		'address' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message'=>'<span class="input-error tooltips" data-original-title="Introduzca una dirección"><i class="icon-exclamation-sign"></i></span>'
			),
		),
		'terrain_size' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message'=>'<span class="input-error tooltips" data-original-title="Introduzca la dimensión del terreno"><i class="icon-exclamation-sign"></i></span>'
			),
		),
		'contract_time' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message'=>'<span class="input-error tooltips" data-original-title="Introduzca la duración del contrato"><i class="icon-exclamation-sign"></i></span>'
			),
		),
		'cost' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => false
			),
			'number' => array(
				'rule' => 'naturalNumber',
				'message' => false
			),
		),
		'discount_amount' => array(
			'number' => array(
				'rule' => 'naturalNumber',
				'allowEmpty' => true,
				'message' => false
			),
		),
		'requirements' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => false
			),
		),
		'latitude' => array(
			'notempty' => array(
				'rule' => array('numeric'),
				'message'=>'<span class="input-error tooltips" data-original-title="Introduzca un valor numérico válido para la latitud"><i class="icon-exclamation-sign"></i></span>',
				'allowEmpty' => true
			),
		),
		'longitude' => array(
			'notempty' => array(
				'rule' => array('numeric'),
				'message'=>'<span class="input-error tooltips" data-original-title="Introduzca un valor numérico válido para la longitud"><i class="icon-exclamation-sign"></i></span>',
				'allowEmpty' => true
			),
		),
		'main_image' => array(
			'checksizeedit' => array(
				'rule' => array('checkSize',false),
				'message' => false,
				'on' => 'update'
				),
			'checktypeedit' =>array(
				'rule' => array('checkType',false),
				'message' => false,
				'on' => 'update'
				),
			'checkuploadedit' =>array(
				'rule' => array('checkUpload', false),
				'message' => false,
				'on' => 'update'
				),
			'checksize' => array(
				'rule' => array('checkSize',true),
				'message' => false,
				'on' => 'create'
				),
			'checktype' =>array(
				'rule' => array('checkType',true),
				'message' => false,
				'on' => 'create'
				),
			'checkupload' =>array(
				'rule' => array('checkUpload', true),
				'message' => false,
				'on' => 'create'
				),
		)
	);

	function checkUpload($data, $required = false){
		$data = array_shift($data);
		if(!$required && $data['error'] == 4){
			return true;
		}
		if($required && $data['error'] !== 0){
			return false;
		}
		if($data['size'] == 0){
			return false;
		}
		return true;
	}

	function checkType($data, $required = false){
		$data = array_shift($data);
		if(!$required && $data['error'] == 4){
			return true;
		}
		$allowedExtensions = array('jpg', 'JPG', 'jpeg', 'JPEG', 'png', 'PNG', 'bmp', 'BMP');
		$extension = pathinfo($data['name'], PATHINFO_EXTENSION);

		if(!in_array($extension, $allowedExtensions)){
			return false;
		}
		return true;
	}

	function checkSize($data, $required = false){
		$data = array_shift($data);
		if(!$required && $data['error'] == 4){
			return true;
		}

		if($data['size'] == 0 ||$data['size']/1024 > 20480){
			return false;
		}
		return true;
	}

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'DefaultImage' => array(
			'className' => 'DefaultImage',
			'foreignKey' => 'foreign_key',
			'dependent' => false,
			'conditions' => array('model'=>'Sale'),
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

	public $belongsTo = array(
		'State' => array(
			'className' => 'State',
			'foreignKey' => 'state_id',
			'dependent' => false
		),
		'City' => array(
			'className' => 'City',
			'foreignKey' => 'city_id',
			'dependent' => false
		)
	);

	// Extra Images Setup
	var $images = array( //mode: raw[default], resizeToWidth, resizeToHeight, resizeToWidthHeight, crop, resizeAndCrop
		'sales' => array(
			'mode' => 'raw',
			'width' => 1020,
			'height' => 360,
			'align' => 'center', //left, center, right
			'valign' => 'middle', //top, middle, bottom
			'thumbnail' => false,
			'view' => true
		)
	);

	function beforeSave() {
		$return = false;
		$image = null;
		$erase = false;
		$sale = null;

		if (!empty($this->Sale->id)) {
			$sale = $this->Sale->find('first', array('conditions' => array('Sale.id' => $this->Sale->data['Sale']['id'])));
			$image = $sale['Sale']['main_image'];
		}
		//File upload
		$file = isset($this->data['Sale']['main_image']) ? $this->data['Sale']['main_image'] : null;
		$fileDir = WWW_ROOT . 'img' . DS . 'images';
		$fileName = '';
		$fileExtension = '';

		//A file was uploaded
		if (!empty($file) && !empty($file['tmp_name']) && $file['error'] != 4) {
			//There is no error, the file was uploaded with success
			if ($file['error'] == 0) {

				//Tells whether the file was uploaded via HTTP POST
				if (is_uploaded_file($file['tmp_name'])) {
					App::import('Vendor', 'Bitmap');
					App::import('Vendor', 'BitmapException');

					if (Bitmap::isImage($file['tmp_name'])) {
						try {
							$fileName = md5($file['tmp_name'] . (rand() * 100000));
							$imageSize = getimagesize($file['tmp_name']);
							$width = $imageSize[0];
							$height = $imageSize[1];

							//Original image
							$img = new Bitmap($file['tmp_name']);
							$img->open();
							$img->save($fileDir, 'sales_img_' . $fileName);
							$img->dispose();
							$fileExtension = $img->getExtension();

							//Image for admin edit preview
							$img_edt = new Bitmap($file['tmp_name']);
							$img_edt->open();
							$img_edt->resizeAndCrop(200, 150);
							$img_edt->save($fileDir, 'sales_img_edt_' . $fileName);
							$img_edt->dispose();
							$fileExtension = $img_edt->getExtension();

							//Image for index table in admin
							$tablePrev = new Bitmap($file['tmp_name']);
							$tablePrev->open();
							$tablePrev->resizeToWidth(145);
							$tablePrev->save($fileDir, 'sales_tbl_prev_' . $fileName);
							$tablePrev->dispose();

							//Image for the main portal main page
							$mainPrev = new Bitmap($file['tmp_name']);
							$mainPrev->open();
							$mainPrev->resizeAndCrop(201, 121);
							$mainPrev->save($fileDir, 'sales_main_prev_' . $fileName);
							$mainPrev->dispose();

							//Image for the portal view page
							$set['height'] = 199;
							$set['width'] = 220;
							$set['align'] = 'center';
							$set['valign'] = 'middle';

							$viewPrev = new Bitmap($file['tmp_name']);
							$viewPrev->open();
							$viewPrev->resizeAndCrop2($imageSize, $set);
							$viewPrev->save($fileDir, 'sales_view_prev_' . $fileName);
							$viewPrev->dispose();

							//Image for the main portal list page
							$listPrev = new Bitmap($file['tmp_name']);
							$listPrev->open();
							$listPrev->resizeAndCrop(300, 271);
							$listPrev->save($fileDir, 'sales_list_prev_' . $fileName);
							$listPrev->dispose();

							//Image for the main portal view page
							$set['height'] = 271;
							$set['width'] = 539;
							$set['align'] = 'center';
							$set['valign'] = 'middle';

							$viewImg = new Bitmap($file['tmp_name']);
							$viewImg->open();
							$viewImg->resizeAndCrop2($imageSize, $set);
							$viewImg->save($fileDir, 'sales_view_img_' . $fileName);
							$viewImg->dispose();

							$this->data['Sale']['size'] = $file['size'];
							$this->data['Sale']['main_image'] = $fileName . '.' .  $fileExtension;
							$return = true;
							$erase = true;
						} catch (BitmapException $e) {
							$this->invalidate('image', 'bitmapError');
						}
					} else {
						$this->invalidate('image', 'unsupportedType');
					}
				} else {
					$this->invalidate('image', 'maliciousUpload');
				}
			} else {
				//File upload error. See http://us3.php.net/manual/en/features.file-upload.errors.php
				$this->invalidate('image', 'uploadError');
			}
		} else {
			if (isset($this->data['Sale']['erase']) && $this->data['Sale']['erase']) {
				$this->data['Sale']['main_image'] = null;
			} else {
				unset($this->data['Sale']['main_image']);
			}
			$return = true;
		}

		// REMOVE PREVIOUS LOGO
		if (!empty($image) && ((isset($this->data['Sale']['erase']) && $this->data['Sale']['erase']) || $erase)) {
			unlink($fileDir . DS . 'sales_main_prev_' . $image);
			unlink($fileDir . DS . 'sales_tbl_prev_' . $image);
			unlink($fileDir . DS . 'sales_img_' . $image);
			unlink($fileDir . DS . 'sales_img_edt_' . $image);
			unlink($fileDir . DS . 'sales_list_prev_' . $image);
			unlink($fileDir . DS . 'sales_view_img_' . $image);
			unlink($fileDir . DS . 'sales_view_prev_' . $image);
		}

		// Joins accepted credits into one string
		if (isset($this->data['Sale']['accepted_credits']) && !empty($this->data['Sale']['accepted_credits'])) {
			$acceptedCredits = implode(", ", $this->data['Sale']['accepted_credits']);
			$this->data['Sale']['accepted_credits'] = $acceptedCredits;
		}

		return $return;
	}

	function beforeDelete() {
		$sale = $this->find('first', array('conditions' => array('Sale.id' => $this->id), 'contain' => false));
		$imageDirectory = WWW_ROOT . 'img' . DS . 'images' . DS;

		if (!empty($sale['Sale']['main_image'])) {
			unlink($imageDirectory . 'sales_img_' . $sale['Sale']['main_image']);
			unlink($imageDirectory . 'sales_main_prev_' . $sale['Sale']['main_image']);
			unlink($imageDirectory . 'sales_tbl_prev_' . $sale['Sale']['main_image']);
			unlink($imageDirectory . 'sales_img_edt_' . $sale['Sale']['main_image']);
			unlink($imageDirectory . 'sales_list_prev_' . $sale['Sale']['main_image']);
			unlink($imageDirectory . 'sales_view_img_' . $sale['Sale']['main_image']);
			unlink($imageDirectory . 'sales_view_prev_' . $sale['Sale']['main_image']);
		}

		if(!empty($sale['DefaultImage'])){
			foreach ($sale['DefaultImage'] as $image) {
				unlink($imageDirectory . 'sales_extra_img_' . $image['image']);
			}
		}

		return true;
	}
}

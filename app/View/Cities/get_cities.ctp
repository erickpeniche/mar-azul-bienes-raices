<?php
	echo $this->Form->input($model.'.city_id',
		array(
			'between' => '<div class="controls input-icon">',
			'options' => $cities,
			'selected' => $cityId,
			'empty' => false,
			'class'=>'m-wrap span3 chosen-with-diselect',
			'label' => array('text'=>'Ciudad', 'class'=>'control-label')
			)
		);
	echo '</div>';
?>
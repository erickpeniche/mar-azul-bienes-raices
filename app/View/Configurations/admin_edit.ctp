<?php
	echo $this->Html->css(array(
		'admin/glyphicons/css/glyphicons',
		), null, array('inline' => false)
	);

	$configuration = $this->request->data;

	$isMap = $configuration['Configuration']['id'] == 4;

	echo $isMap ? $this->Html->script(array(
	   'http://maps.google.com/maps/api/js?sensor=false',
	   'admin/js/gmaps',
	   'admin/View/Configurations/admin_edit'
	   ), array('inline' => false)
	) : '';

	$this->set('activeMenu', 'configurations');

	$this->Html->addCrumb('Configuraciones',
		array(
			'action' => 'index',
			'controller' => 'configurations',
			'admin' => true
			)
		);

	$this->Html->addCrumb($configuration['Configuration']['name'], null);
	$this->Html->addCrumb('Editar', null);

?>

<div class="portlet box blue">
   <div class="portlet-title">
	  <h4><i class="icon-cog"></i>Editar configuración</h4>
	  <div class="actions">
		 <?php echo $this->Html->link('<i class="icon-cog"></i> Configuraciones</a>',
			array(
			   'action' => 'index',
			   'controller' => 'configurations',
			   'admin' => true
			   ),
			array(
			   'class' => 'btn yellow',
			   'escape' => false)
			);
		 ?>
	  </div>
   </div>
	<div class="portlet-body form">
		<!-- BEGIN FORM-->
		<?php
			echo $this->Form->create('Configuration',
				array(
					'inputDefaults' => array(
						'div' => array('class' => 'control-group'),
						'class' => array('class' => 'm-wrap span12')
						),
					'class' => 'form-horizontal form-row-seperated',
					'novalidate' => true
					)
				);
			echo $this->Form->input('id', array('type' => 'hidden'));
		?>
		<?php if($isMap): ?>
		<div class="control-group required">
			<label for="ConfigurationValue" class="control-label">Google Maps<span class="required">*</span></label>
			<div class="controls input-icon">
				<div class="label label-important visible-ie8">Not supported in Internet Explorer 8</div>
				<div id="gmap_marker" class="gmaps"></div>
				<div id="map-info">
					<?php
						echo $this->Form->input('value', array('type' => 'hidden'));

						echo $this->Form->button('<i class="icon-undo"></i> Limpiar',
							array('type' => 'button', 'class' => 'btn', 'id'=>'clean-map')
						);
					?>
				</div>
			</div>
		</div>
		<?php else:
				$div = $this->Html->tag('div', null, array('class'=>'controls input-icon'));
				echo $this->Form->input('value',
					array(
						'between' => $div,
						'placeholder'=>'Escriba un valor para esta configuración',
						'data-required'=>1,
						'label' => array(
							'text'=>$configuration['Configuration']['name'].'</a><span class="required">*</span>',
							'class'=>'control-label'
						)
					)
				);
				echo '</div>';
			endif;
		?>

		<?php
			echo '<div class="form-actions">';
			echo $this->Form->button('<i class="icon-ok"></i> Guardar',
				array('type' => 'submit', 'class' => 'btn green')
				);
			echo '&nbsp;';

			$cancelOptions = array('action' => 'index', 'controller' => 'configurations', 'admin' => true);
			echo $this->Form->button('Cancelar',
				array(
					'type' => 'button',
					'class' => 'btn red',
					'onclick' => "location.href='".$this->Html->url($cancelOptions)."'"
					)
				);
			echo '</div>';
		?>
		<?php echo $this->Form->end(); ?>
		<!-- END FORM-->
	</div>
</div>
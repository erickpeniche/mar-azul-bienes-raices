<?php
	echo $this->Html->css(array(
		'admin/bootstrap-fileupload/bootstrap-fileupload',
		'admin/gritter/css/jquery.gritter',
		'admin/jquery-tags-input/jquery.tagsinput',
		'admin/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons',
		'admin/data-tables/DT_bootstrap',
		), null, array('inline' => false)
	);

	echo $this->Html->script(array(
	//'admin/js/jquery-1.8.3.min',
		'admin/bootstrap-fileupload/bootstrap-fileupload',
		'admin/jquery-tags-input/jquery.tagsinput.min',
		'admin/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons'
		), array('inline' => false)
	);

	if (strtolower($model) == 'rent') {
		$this->Html->addCrumb('Rentas',
			array(
				'action' => 'index',
				'controller' => 'rents',
				'admin' => true
				)
			);
		$this->Html->addCrumb($foreignKey,
			array(
				'action' => 'view',
				'controller' => 'rents',
				$foreignKey,
				'admin' => true
				)
			);
		$this->set('activeMenu', 'rents');
		$buttonLink = $this->Html->link('<i class="icon-tag"></i> Rentas</a>',
			array(
			   'action' => 'index',
			   'controller' => 'rents',
			   'admin' => true
			   ),
			array(
			   'class' => 'btn purple',
			   'escape' => false)
			);
	} else {
		$this->Html->addCrumb('Ventas',
			array(
				'action' => 'index',
				'controller' => 'sales',
				'admin' => true
				)
			);
		$this->Html->addCrumb($foreignKey,
			array(
				'action' => 'view',
				'controller' => 'sales',
				$foreignKey,
				'admin' => true
				)
			);
		$this->set('activeMenu', 'sales');
		$buttonLink = $this->Html->link('<i class="icon-money"></i> Ventas</a>',
			array(
			   'action' => 'index',
			   'controller' => 'sales',
			   'admin' => true
			   ),
			array(
			   'class' => 'btn green',
			   'escape' => false)
			);
	}

	$this->Html->addCrumb('Galería de imágenes', null);

	$this->Html->addCrumb('Agregar', null);
?>
<div class="portlet box blue">
   <div class="portlet-title">
	  <h4><i class="icon-picture"></i>Nueva imagen</h4>
	  <div class="actions">
		 <?php echo $buttonLink;
		 ?>
	  </div>
   </div>
   <div class="portlet-body form">
	  <!-- BEGIN FORM-->
	  <?php echo $this->Form->create('DefaultImage',
		 array(
			'inputDefaults' => array(
			   'div' => array('class' => 'control-group'),
			   'class' => array('class' => 'm-wrap span12')
			   ),
			'class' => 'form-horizontal form-row-seperated',
			'enctype' => 'multipart/form-data',
			'novalidate' => true
			)
		 );
	  ?>
	  <?php
		 echo $this->Form->hidden('model', array('value' => $this->params['pass'][0]));
		 echo $this->Form->hidden('foreign_key', array('value' => $this->params['pass'][1]));

		 $div = $this->Html->tag('div', null, array('class'=>'controls'));
		 $help = $this->Html->tag('span', '*Opcional - Es una palabra u oración corta que describa la imagen', array('class'=>'help-inline'));
		 echo $this->Form->input('description',
			array(
			   'between' => $div,
			   'placeholder'=>'Descripción breve de la imagen',
			   'label' => array('text'=>'Descripción', 'class'=>'control-label'),
			   'after' => $help
			   )
			);
		 echo '</div>';
	  ?>

		<div class="control-group <?php echo isset($this->validationErrors['DefaultImage']['file']) ? 'error' : ''; ?>">
			<label class="control-label">Imagen<span class="required">*</span></label>
			<div class="controls">
				<div class="fileupload fileupload-new" data-provides="fileupload">
					<div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
						<?php echo $this->Html->image('http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=sin+imagen'); ?>
					</div>
					<div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
					<div>
						<span class="btn btn-file">
							<span class="fileupload-new">Seleccionar</span>
							<span class="fileupload-exists">Cambiar</span>
							<?php echo $this->Form->file('file', array('class' => 'default')); ?>
						</span>
						<a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Eliminar</a>
					</div>
				</div>
				<span class="help-block"><em>Es la imagen que aparecerá en la portada del anuncio en la página principal</em></span>
				<span class="label label-important">NOTA!</span>
				<span>La imagen debe pesar menos de <strong>20 MB</strong> y debe ser formato <strong>JPG, PNG o BMP</strong></span>
			</div>
		</div>

	  <?php
		 echo '<div class="form-actions">';
		 echo $this->Form->button('<i class="icon-ok"></i> Guardar',
			array('type' => 'submit', 'class' => 'btn green')
			);
		 echo '&nbsp;';

		 $cancelOptions = array('action' => 'index', 'controller' => 'default_images', 'admin' => true);
		 echo $this->Form->button('Cancelar',
			array(
			   'type' => 'button',
			   'class' => 'btn red',
			   'onclick' => "location.href='".$this->Html->url($cancelOptions)."'"
			   )
			);
		 echo '</div>';
	  ?>

	  <?php echo $this->Form->end(); ?>
	  <!-- END FORM-->
   </div>
</div>
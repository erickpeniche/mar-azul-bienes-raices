<?php
	echo $this->Html->scriptBlock(
		'$.gritter.add({
				// (string | mandatory) the heading of the notification
				title: '."'Error!'".',
				// (string | mandatory) the text inside the notification
				text: '."'".$message."'".',
				// (string | optional) the image to display on the left
				image: '."'/img/icons/no.png'".',
				// (bool | optional) if you want it to fade out on its own or just sit there
				sticky: false,
				// (int | optional) the time you want it to be alive for before fading out
				time: '."''".'
			});',
		array('inline' => false)
	);
?>
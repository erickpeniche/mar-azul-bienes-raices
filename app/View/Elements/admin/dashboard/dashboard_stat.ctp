<div class="span3 responsive" data-tablet="span6" data-desktop="span3">	
	<div class="dashboard-stat <?php echo $color;?>">
		<div class="visual">
			<i class="icon-<?php echo $icon;?>"></i>
		</div>
		<div class="details">
			<?php
				echo $this->Html->tag('div', $number, array('class'=>'number'));  
			?>
			<?php
				echo $this->Html->tag('div', $title, array('class'=>'desc'));  
			?>
		</div>
		<?php
			echo $this->Html->link(
				'Ver <i class="m-icon-swapright m-icon-white"></i>',
				array(
					'action' => 'index',
					'controller' => $ctrlr,
					'admin' => true
					),
				array(
					'class' => 'more',
					'escape' => false
					)
				);
		?>
	</div>
</div>
<?php
	$activeMenus = array(
		'home' => array(
			'active' => $activeMenu == 'home' ? 'active' : '',
			'selected' => $activeMenu == 'home' ? '<span class="selected"></span>' : ''
			),
		'mainImages' => array(
			'active' => $activeMenu == 'mainImages' ? 'active' : '',
			'selected' => $activeMenu == 'mainImages' ? '<span class="selected"></span>' : ''
			),
		'rents' => array(
			'active' => $activeMenu == 'rents' ? 'active' : '',
			'selected' => $activeMenu == 'rents' ? '<span class="selected"></span>' : ''
			),
		'sales' => array(
			'active' => $activeMenu == 'sales' ? 'active' : '',
			'selected' => $activeMenu == 'sales' ? '<span class="selected"></span>' : ''
			),
		'subscribers' => array(
			'active' => $activeMenu == 'subscribers' ? 'active' : '',
			'selected' => $activeMenu == 'subscribers' ? '<span class="selected"></span>' : ''
			),
		'users' => array(
			'active' => $activeMenu == 'users' ? 'active' : '',
			'selected' => $activeMenu == 'users' ? '<span class="selected"></span>' : ''
			),
		'configurations' => array(
			'active' => $activeMenu == 'configurations' ? 'active' : '',
			'selected' => $activeMenu == 'configurations' ? '<span class="selected"></span>' : ''
			)
	);
 ?>
<!-- BEGIN SIDEBAR MENU -->
<ul>
	<li>
		<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
		<div class="sidebar-toggler hidden-phone"></div>
		<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
	</li>
	<li>
		&nbsp;
		<!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
		<!-- <form class="sidebar-search">
			<div class="input-box">
				<a href="javascript:;" class="remove"></a>
				<input type="text" placeholder="Buscar..." />
				<input type="button" class="submit" value=" " />
			</div>
		</form> -->
		<!-- END RESPONSIVE QUICK SEARCH FORM -->
	</li>
	<li class="start <?php echo $activeMenus['home']['active']; ?>">
		<?php
			echo $this->Html->link(
				'<i class="icon-home"></i><span class="title">Inicio</span>'.$activeMenus['home']['selected'],
				array(
					'controller' => 'pages',
					'action' => 'home',
					'admin' => true
					),
				array(
					'escape' => false
					)
				);
		?>
	</li>
	<li class="<?php echo $activeMenus['mainImages']['active']; ?>">
		<?php
			echo $this->Html->link(
				'<i class="icon-picture"></i><span class="title">Imágenes principales</span>'.$activeMenus['mainImages']['selected'],
				array(
					'action' => 'index',
					'controller' => 'main_images',
					'admin' => true
					),
				array(
					'escape' => false
					)
				);
		?>
	</li>
	<li class="<?php echo $activeMenus['rents']['active']; ?>">
		<?php
			echo $this->Html->link(
				'<i class="icon-tag"></i><span class="title">Rentas</span>'.$activeMenus['rents']['selected'],
				array(
					'controller' => 'rents',
					'action' => 'index',
					'admin' => true,
					),
				array(
					'escape' => false
					)
				);
		?>
	</li>
	<li class="<?php echo $activeMenus['sales']['active']; ?>">
		<?php
			echo $this->Html->link(
				'<i class="icon-money"></i><span class="title">Ventas</span>'.$activeMenus['sales']['selected'],
				array(
					'controller' => 'sales',
					'action' => 'index',
					'admin' => true,
					),
				array(
					'escape' => false
					)
				);
		?>
	</li>
	<li class="<?php echo $activeMenus['subscribers']['active']; ?>">
		<?php
			echo $this->Html->link(
				'<i class="icon-envelope"></i><span class="title">Suscriptores y boletín</span>'.$activeMenus['subscribers']['selected'],
				array(
					'controller' => 'subscribers',
					'action' => 'index',
					'admin' => true,
					),
				array(
					'escape' => false
					)
				);
		?>
	</li>
	<?php if($isSuperUser): ?>
	<li class="<?php echo $activeMenus['users']['active']; ?>">
		<?php
			echo $this->Html->link(
				'<i class="icon-user"></i><span class="title">Usuarios</span>'.$activeMenus['users']['selected'],
				array(
					'action' => 'index',
					'controller' => 'users',
					'admin' => true
					),
				array(
					'escape' => false
					)
				);
		?>
	</li>
	<li class="<?php echo $activeMenus['configurations']['active']; ?>">
		<?php
			echo $this->Html->link(
				'<i class="icon-cog"></i><span class="title">Configuraciones</span>'.$activeMenus['configurations']['selected'],
				array(
					'controller' => 'configurations',
					'action' => 'index',
					'admin' => true
					),
				array(
					'escape' => false
					)
				);
		?>
	</li>
	<?php endif; ?>
</ul>
<!-- END SIDEBAR MENU -->
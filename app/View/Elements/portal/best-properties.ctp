<div class="grid_3 omega">
	<h3 class="p4">Destacadas</h3>
<?php if(!empty($bestRents)): ?>
	<?php foreach($bestRents as $rent): ?>
	<div class="box2 p2">
		<div class="indents">
			<figure class="p2-1">
				<?php
					$img = $this->Html->image('images/rents_main_prev_'.$rent['Rent']['main_image']);
					echo $this->Html->link($img, array('controller'=>'rents', 'action'=>'view', $rent['Rent']['id']), array('escape'=>false));
				?>
			</figure>
			<strong><?php echo $rent['Rent']['type']. ' en ' . $rent['City']['name'] . ', ' . $rent['State']['name']; ?></strong>
			Terreno:&nbsp;&nbsp;<?php echo $rent['Rent']['terrain_size']; ?> <br>
			Costo:&nbsp;&nbsp;<?php echo $this->Number->currency($rent['Rent']['cost'], 'USD'); ?>
		</div>
	</div>
	<?php endforeach; ?>
<?php endif; ?>
	<div class="box2">
		<div class="indents">
			<figure class="p2-1">
				<?php
					$img = $this->Html->image('images/sales_main_prev_'.$bestSale['Sale']['main_image']);
					echo $this->Html->link($img, array('controller'=>'sales', 'action'=>'view', $bestSale['Sale']['id']), array('escape'=>false));
				?>
			</figure>
			<strong><?php echo $bestSale['Sale']['type']. ' en ' . $bestSale['City']['name'] . ', ' . $bestSale['State']['name']; ?></strong>
			Terreno:&nbsp;&nbsp;<?php echo $bestSale['Sale']['terrain_size']; ?> <br>
			Costo:&nbsp;&nbsp;<?php echo $this->Number->currency($bestSale['Sale']['cost'], 'USD'); ?>
		</div>
	</div>
</div>
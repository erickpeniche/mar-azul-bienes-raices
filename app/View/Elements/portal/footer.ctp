<footer>
	<div class="footer-bg">
		<div class="container_12">
			<div class="wrapper">
				<div class="grid_6">
					<div class="wrapper">
						<div class="dgrid_3">
							<h3 class="p3">Suscríbete al boletín</h3>
							<ul class="footer-menu p5">
								<li>
									<?php
										echo $this->Form->create('Subscriber', array('inputDefaults' => array('div' => false),'novalidate' => true, 'id'=>'subscribe-form'));
									?>
									<span>Correo electrónico</span>
									<label class="email">
										<?php
											echo $this->Form->input('email',
												array(
													'label'=>false,
													'after'=>'<span class="error">*No es una dirección de correo electrónico válida.</span><span class="empty">*Escribe una dirección de correo electrónico.</span>'
												)
											);
										?>
									</label>
									<div class="success">
										<strong>Te haz suscrito exitosamente!</strong>
									</div>

									<div class="buttons-wrapper">
										<?php echo $this->Html->link('Suscribir', '#', array('class'=>'subscribe-button', 'data-type'=>'submit', 'data-form'=>'subscribe')); ?>
									</div>
									<?php echo $this->Form->end(); ?>
								</li>
							</ul>
						</div>
						<div class="dgrid_3">
							<h3 class="p3">Síguenos</h3>
							<ul class="social-icons">
								<li>
									<?php
										$facebook = Configure::read('App.configurations.social-networks-facebook');
										$link = !empty($facebook) ? $facebook : '#';
										echo $this->Html->link('', $link, array('class'=>'icon-1'));
									?>
								</li>
								<li class="last-item">
									<?php
										$twitter = Configure::read('App.configurations.social-networks-twitter');
										$link = !empty($twitter) ? $twitter : '#';
										echo $this->Html->link('', $link, array('class'=>'icon-3'));
									?>
							</ul>
						</div>
					</div>
				</div>
				<div class="grid_6 last-col">
					<div class="wrapper">
						<div class="dgrid_3">
							<h3 class="p3">Mapa del sitio</h3>
							<ul class="footer-menu">
								<li><?php echo $this->Html->link('Inicio', array('controller'=>'pages', 'action'=>'home')); ?></li>
								<li><?php echo $this->Html->link('Nosotros', array('controller'=>'pages', 'action'=>'about_us')); ?></li>
								<li><?php echo $this->Html->link('Rentas', array('controller'=>'rents', 'action'=>'index')); ?></li>
								<li class="last-item"><?php echo $this->Html->link('Ventas', array('controller'=>'sales', 'action'=>'index')); ?></li>
							</ul>
						</div>
						<div class="dgrid_3">
							<h3 class="p3">Contacto</h3>
							<ul class="footer-menu">
								<li class="last-item"><?php echo $this->Text->autoLinkEmails(Configure::read('App.configurations.contact-email')); ?></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container_12">
		<div class="wrapper">
			<div class="grid_12">
				<div class="footer-text">
					Mar Azul Bienes Raíces &copy; <?php echo date('Y') ?> | <a href="index-7.html">Política de Privacidad</a>
					<div class="footer-link"><!--{%FOOTER_LINK} --></div>
				</div>
			</div>
		</div>
	</div>
</footer>
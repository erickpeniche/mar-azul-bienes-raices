<?php
	/**
	 * Dynamic navigation highlighting
	 * Each page sets a variable $this->set('name_of_page', 'active');
	 * If the variable is set, then we are on that page, so the active class is set for that link
	 */
	$home = isset($home) ? $home : '';
	$about_us = isset($about_us) ? $about_us : '';
	$rents = isset($rentsMenu) ? $rentsMenu : '';
	$sales = isset($salesMenu) ? $salesMenu : '';
	$contact = isset($contact) ? $contact : '';
?>
<nav>
	<div id="menu-icon">Menu</div>
	<ul class="menu responsive-menu">
		<li class="<?php echo $home; ?>">
			<?php echo $this->Html->link('Inicio', array('controller' => 'pages', 'action' => 'home')); ?>
		</li>
		<li class="<?php echo $about_us; ?>">
			<?php echo $this->Html->link('Nosotros', array('controller' => 'pages', 'action' => 'about_us')); ?>
		</li>
		<li class="<?php echo $rents; ?>">
			<?php echo $this->Html->link('Renta', array('controller' => 'rents', 'action' => 'index')); ?>
		</li>
		<li class="<?php echo $sales; ?>">
			<?php echo $this->Html->link('Venta', array('controller' => 'sales', 'action' => 'index')); ?>
		</li>
		<li class="last-item <?php echo $contact; ?>">
			<?php echo $this->Html->link('Contacto', array('controller' => 'pages', 'action' => 'contact')); ?>
		</li>
	</ul>
</nav>
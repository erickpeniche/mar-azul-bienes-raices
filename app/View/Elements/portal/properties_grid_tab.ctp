<?php
	$wrapper = 0;
	$grid6 = 0;
	$boxes = 0;
?>

<!-- BEGIN TAB GRID CONTENT -->
<?php if(!empty($data)): ?>
<div id="<?php echo $tabId; ?>" class="tab_content">
	<?php $boxes = 0; ?>
	<?php foreach($data as $property): ?>
		<?php $boxes++ ?>
		<?php if ($boxes%4 == 1) : ?>
			<?php $wrapper++;?>
		<div class="wrapper <?php echo $wrapper<3 ? 'indent-bottom9' : ''; ?>">
		<?php endif ?>
			<?php if ($boxes%2 == 1) : ?>
				<?php $grid6++; ?>
			<div class="grid_6 <?php echo $grid6 == 1 ? 'alpha' : 'omega'; ?>">
				<div class="wrapper">
			<?php endif ?>
					<div class="dgrid_3">
						<div class="box2">
							<?php if($tabId == 'tab5'): ?>
							<div class="ribbon-wrapper-green">
								<div class="ribbon-green">-<?php echo $property[$model]['discount_amount']; ?>%</div>
							</div>
							<?php endif; ?>
							<div class="indents">
								<figure class="p2-1">
									<?php
										$img = $this->Html->image('images/'.$imgPrefix.'_main_prev_'.$property[$model]['main_image']);
										echo $this->Html->link($img, array('controller'=>$imgPrefix, 'action'=>'view', $property[$model]['id']), array('escape'=>false));
									?>
								</figure>
								Terreno:&nbsp;&nbsp;<?php echo $property[$model]['terrain_size']; ?> <br>
								Costo:&nbsp;&nbsp;<?php echo $this->Number->currency($property[$model]['cost'], 'USD'); ?>
							</div>
						</div>
					</div>
			<?php if ($boxes%2 == 0 || sizeof($data) == $boxes) : ?>
				<?php if($grid6 == 2){$grid6 = 0;} ?>
				</div>
			</div>
			<?php endif; ?>
		<?php if ($boxes%4 == 0 || sizeof($data) == $boxes) : ?>
		</div>
		<?php endif ?>
	<?php endforeach; ?>
</div>
<?php else: ?>
<div id="<?php echo $tabId; ?>" class="tab_content">
	<div class="wrapper indent-bottom9">
		<div class="grid_12 alpha">
			<div class="wrapper">
				<h4><em>No se encuentran propiedades en esta categoría</em></h4>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>
<?php
	$totalProperties = count($data);
	$totalWrappers = round($totalProperties / 3);

	$wrapper = 0;
	$boxClass = '';
	$boxes = 0;
?>

<!-- BEGIN TAB GRID CONTENT -->
<?php if(!empty($data)): ?>
	<?php foreach($data as $property): ?>
		<?php $boxes++ ?>
		<?php if ($boxes%3 == 1) : ?>
			<?php $wrapper++;?>
		<div class="wrapper indent-bottom<?php echo $wrapper!=$totalWrappers ? '7-1' : '3'; ?>">
		<?php endif ?>
			<?php
				switch ($boxes) {
					case 1:
						$boxClass = 'alpha';
						break;
					case 2:
						$boxClass = '';
						break;
					case 3:
						$boxClass = 'omega';
						break;
				}
			?>
			<div class="grid_3 <?php echo $boxClass; ?>">
				<figure class="p5" id="propertythn">
					<?php if(isset($property[$model]['rented']) && $property[$model]['rented']): ?>
					<div class="ribbon-wrapper-green">
						<div class="ribbon-red">Rentada</div>
					</div>
					<?php elseif(isset($property[$model]['discount_available']) && $property[$model]['discount_available']): ?>
					<div class="ribbon-wrapper-green">
						<div class="ribbon-green" style="font-size: 1em">-<?php echo $property[$model]['discount_amount']; ?>%</div>
					</div>
					<?php elseif(isset($property[$model]['best_offer']) && $property[$model]['best_offer']): ?>
					<div class="ribbon-wrapper-green">
						<div class="ribbon-green" style="font-size: 1em">Destacada</div>
					</div>
					<?php endif; ?>
					<?php
						$img = $this->Html->image('images/'.$prevPrefix.$property[$model]['main_image']);
						echo $this->Html->link($img, array('controller'=>$folder, 'action'=>'view', $property[$model]['id']), array('class'=>'lightbox-image', 'escape'=>false))
					?>
				</figure>
				<h6>
					<?php
						$title = $property[$model]['type'].' en '.$property['City']['name'].', '.$property['State']['name'];
						echo $this->Html->link($title, array('controller'=>$folder, 'action'=>'view', $property[$model]['id']));
					?>
				</h6>
				<?php echo $this->Text->truncate(strip_tags($property[$model]['description']), 115, array('ellipsis'=>'...', 'exact'=>true)); ?>
			</div>
		<?php if ($boxes%3 == 0 || sizeof($data) == $boxes) : ?>
			<?php $boxes = 0; ?>
		</div>
		<?php endif ?>
	<?php endforeach; ?>
<?php else: ?>
<h3 class="p5"><em>No se encuentran propiedades disponibles</em></h3>
<?php endif; ?>
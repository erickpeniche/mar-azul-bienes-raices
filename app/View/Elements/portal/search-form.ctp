<!--=============== Form ==================-->
<?php
	echo $this->Form->create($model,
		array(
			'action'=>'search',
			'inputDefaults' => array(
				'div' => false,
				'label' => false
			),
			'class' => 'jqtransform',
			'id' => 'full-search-form',
			'novalidate' => true
		)
	);
?>
	<fieldset>
		<label class="city">
			<span class="label">Ciudad ó Estado:</span>
			<?php echo $this->Form->input('state_city', array('class'=>'stateCity')); ?>
		</label>
		<label class="area">
			<span class="label">Tipo de propiedad</span>
			<?php
				$types = $model == 'Rent' ? array('0' => 'Cualquiera', '1' => 'Casa', '2' => 'Departamento') : array('0' => 'Cualquiera', '1' => 'Casa', '2' => 'Departamento', '3' => 'Terreno');
				echo $this->Form->input('type', array('options' => $types, 'default' => '0'));
			?>
		</label>
		<div>
			<label class="min-price">
				<span class="label">Rango de costo</span>
				<?php echo $this->Form->input('min_price', array('class'=>'minPriceCustom')); ?>
			</label>
			<label class="max-price">
				<span class="label">a</span>
				<?php echo $this->Form->input('max_price', array('class'=>'maxPriceCustom')); ?>
			</label>
			<div class="clear"></div>
		</div>
		<label class="floors">
			<span class="label">Planta(s)</span>
			<?php
				$types = array('0' => 'Cualquiera', '1' => '1', '2' => '2', '3' => '3+');
				echo $this->Form->input('floors', array('options' => $types, 'default' => '0'));
			?>
		</label>
		<label class="bedrooms">
			<span class="label">Recámara(s)</span>
			<?php
				$types = array('0' => 'Cualquiera', '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5+');
				echo $this->Form->input('bedrooms', array('options' => $types, 'default' => '0'));
			?>
		</label>
		<label class="bathrooms">
			<span class="label">Baño(s)</span>
			<?php
				$types = array('0' => 'Cualquiera', '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5+');
				echo $this->Form->input('bathrooms', array('options' => $types, 'default' => '0'));
			?>
		</label>
		<div class="button-wrapper"><a class="button">Buscar</a></div>
	</fieldset>
<?php echo $this->Form->end(); ?>
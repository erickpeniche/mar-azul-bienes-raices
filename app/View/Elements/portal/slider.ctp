<!--=================== slider ==================-->
<div class="slider-bg">
	<div class="container_12">
		<div class="wrapper">
			<div class="grid_12">
				<div id="slides">
					<div class="slides_container">
						<?php foreach($mainImages as $mainImage): ?>
						<div class="slide">
							<?php echo $this->Html->image('images/main_home_'.$mainImage['MainImage']['image'], array('alt' => '')); ?>
						</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
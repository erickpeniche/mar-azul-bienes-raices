<?php $imgDir = $this->webroot.IMAGES_URL; ?>
<html lang="sp">
<head>
	<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
	<title>Contacto</title>
</head>
<body marginheight="0" topmargin="0" marginwidth="0" bgcolor="#E3E3E3" offset="0" leftmargin="0">

	<!-- wrapper table - needed as some readers strip the <html>, <head> and <body> tags -->
	<table background="<?php echo $imgDir.'email/bgMA.png'; ?>" cellspacing="0" style="font-size: 12px; line-height: 20px; font-family: Arial, sans-serif; color: #fff;" bgcolor="#E3E3E3" width="100%" cellpadding="0">
		<tr>
			<td>
				<!-- ///////////////////////////////////// Newsletter Content  ///////////////////////////////////// -->
				<br />

				<!-- START HEADER CONTENT -->
				<table rules="none" cellspacing="0" border="0" align="center" style="font-size: 12px; line-height: 20px; font-family: Arial, sans-serif; color: #fff;" width="600" cellpadding="0">
					<tr>
						<td>
							<table cellspacing="0" align="center" style="font-size: 12px; line-height: 20px; font-family: Arial, sans-serif; color: #fff;" width="560" cellpadding="0">
								<tr>
									<td class="logo" valign="top" width="160">
										<a href="http://www.marazulbienesraices.com/" style="color: #2A5DB0;">
											<img src="<?php echo $this->Html->url($imgDir.'email/logo.png', true); ?>" border="0" height="57" alt="" width="182" />
										</a>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<!-- END HEADER CONTENT -->

				<br />

				<table rules="none" cellspacing="0" border="1" bordercolor="#d6d6d6" frame="border" align="center" style="font-size: 12px; border-color: #d6d6d6 #d6d6d6 #d6d6d6 #d6d6d6; border-collapse: collapse; font-family: Arial, sans-serif; line-height: 20px; color: #666666; border-spacing: 0px; border-width: 1px 1px 1px 1px; border-style: solid solid solid solid; background: #ffffff;" bgcolor="#ffffff" width="600" cellpadding="0">
					<tr>
						<td>
							<img src="<?php echo $this->Html->url($imgDir.'email/module-divider-3.gif', true); ?>" height="30" alt="" width="600" />

							<!-- START MAIN CONTENT. ADD MODULES BELOW -->

							<!-- Module #6 | 2 columns, 352px, 20px spacer, 166px  -->
							<table cellspacing="0" align="center" style="font-size: 12px; line-height: 20px; font-family: Arial, sans-serif; color: #666666;" width="540" cellpadding="0">
								<tr>
									<td valign="top" width="540">
										<h3 style="font-size: 16px; font-family: Arial, sans-serif; color: #000;">Datos de contacto:</h3>

										<ul style="padding-left: 16px; margin-left: 0;">
											<li>Nombre: <b><?php echo $contactName; ?></b></li>
											<li>E-mail: <b><?php echo $contactMail; ?></b></li>
											<li>Teléfono: <b><?php echo $contactPhone; ?></b></li>
										</ul>
									</td>
								</tr>

								<tr>
									<td valign="top">
										<h3 style="font-size: 16px; font-family: Arial, sans-serif; color: #000;">Mensaje:</h3>
										<p><?php echo $contactMessage; ?></p>
									</td>
								</tr>
							</table>

							<!-- END MAIN CONTENT -->
							<!-- START SOCIAL + COMPANY INFO + SUBSCRIPTION -->

							<?php
								echo $this->Html->image($this->Html->url($imgDir.'email/module-divider-3.gif', true), array('style'=>'height: 30px; width: 600px'));
							?>

							<table cellspacing="0" style="font-size: 12px; border-bottom: 1px solid #D0D0D0; font-family: Arial, sans-serif; line-height: 20px; color: #666666; border-top: 1px solid #D0D0D0;" bgcolor="#efefef" cellpadding="0">
								<tr>
									<td height="45" valign="middle" width="30">&nbsp;</td>
									<td height="45" valign="middle" width="185"><b style="color: #333333;">Conéctate con nosotros:</b></td>
									<td height="45" valign="middle" width="25">
										<a href="<?php echo !empty($confs['social-networks-facebook']) ? $confs['social-networks-facebook'] : ''; ?>" title="" style="color: #3a3a3a; text-decoration: none;">
											<?php
												echo $this->Html->image($this->Html->url($imgDir.'email/social/facebook.png', true), array('style'=>'height: 24px; width: 24px; border: 0'));
											?>
										</a>
									</td>
									<td height="45" valign="middle" width="165">
										&nbsp;&nbsp;
										<a href="<?php echo !empty($confs['social-networks-facebook']) ? $confs['social-networks-facebook'] : ''; ?>" title="" style="color: #3a3a3a; text-decoration: none;">Facebook</a>
									</td>
									<td height="45" valign="middle" width="25">
										<a href="<?php echo !empty($confs['social-networks-twitter']) ? $confs['social-networks-twitter'] : ''; ?>" title="" style="color: #3a3a3a; text-decoration: none;">
											<?php
												echo $this->Html->image($this->Html->url($imgDir.'email/social/twitter.png', true), array('style'=>'height: 24px; width: 24px; border: 0'));
											?>
										</a>
									</td>
									<td height="45" valign="middle" width="170">
										&nbsp;&nbsp;
										<a href="<?php echo !empty($confs['social-networks-twitter']) ? $confs['social-networks-twitter'] : ''; ?>" title="" style="color: #3a3a3a; text-decoration: none;">Twitter</a>
									</td>
								</tr>
							</table>

							<?php
								echo $this->Html->image($this->Html->url($imgDir.'email/module-divider-3.gif', true), array('style'=>'height: 30px; width: 600px'));
							?>

							<table cellspacing="0" align="center" style="font-size: 12px; line-height: 20px; font-family: Arial, sans-serif; color: #666666;" width="540" cellpadding="0">
								<tr>
									<td valign="top" width="540">
										<p>
											<b style="color: #333333;"><?php echo !empty($confs['website-title']) ? $confs['website-title'] : ''; ?></b>
											<br /><?php echo !empty($confs['contact-address']) ? $confs['contact-address'] : ''; ?>
											<br /><?php echo !empty($confs['contact-phone']) ? $confs['contact-phone'] : ''; ?>
											<br /><a href="mailto:<?php echo !empty($confs['contact-email']) ? $confs['contact-email'] : ''; ?>" style="color: #2A5DB0;"><?php echo !empty($confs['contact-email']) ? $confs['contact-email'] : ''; ?></a>
										</p>
									</td>
								</tr>
							</table>

							<?php
								echo $this->Html->image($this->Html->url($imgDir.'email/module-divider-3.gif', true), array('style'=>'height: 30px; width: 600px'));
							?>
							<!-- END SOCIAL + COMPANY INFO + SUBSCRIPTION -->
						</td>
					</tr>
				</table>

				<br />

				<table cellspacing="0" align="center" style="font-size: 12px; line-height: 20px; font-family: Arial, sans-serif; color: #666666;" width="560" cellpadding="0">
					<tr>
						<td align="center" valign="top">
							<!-- START FOOTER CONTENT -->
							<p>&copy; <?php echo date('Y') ?> <b style="color: #333333;"><?php echo !empty($confs['website-title']) ? $confs['website-title'] : ''; ?></b>. Todos los derechos reservados.</p>
							<!-- END FOOTER CONTENT -->
						</td>
					</tr>
				</table>
				<br />
				<!-- ///////////////////////////////////// End Newsletter Content  ///////////////////////////////////// -->
			</td>
		</tr>
	</table><!-- end wrapper table-->
</body>
</html>
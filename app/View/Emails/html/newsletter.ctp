<?php $imgDir = $this->webroot.IMAGES_URL; ?>
<html lang="sp">
<head>
	<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
	<title>MarAzul Bienes Raíces - Newsletter</title>
</head>
<body marginheight="0" topmargin="0" marginwidth="0" bgcolor="#E3E3E3" offset="0" leftmargin="0">

	<!-- wrapper table - needed as some readers strip the <html>, <head> and <body> tags -->
	<table background="<?php echo $imgDir.'email/bgMA.png'; ?>" cellspacing="0" style="font-size: 12px; line-height: 20px; font-family: Arial, sans-serif; color: #fff;" bgcolor="#E3E3E3" width="100%" cellpadding="0">
		<tr>
			<td>
				<!-- ///////////////////////////////////// Newsletter Content  ///////////////////////////////////// -->
				<br />

				<!-- START HEADER CONTENT -->
				<table rules="none" cellspacing="0" border="0" align="center" style="font-size: 12px; line-height: 20px; font-family: Arial, sans-serif; color: #fff;" width="600" cellpadding="0">
					<tr>
						<td>
							<table cellspacing="0" align="center" style="font-size: 12px; line-height: 20px; font-family: Arial, sans-serif; color: #fff;" width="560" cellpadding="0">
								<tr>
									<td  valign="top" style="font-size: 11px; line-height: 14px;" width="352"></td>
									<td align="right" valign="middle" style="font-size: 11px; line-height: 14px;" width="206">
										<p>
											Visita nuestro sitio web
											<br /><a href="http://www.marazulbienesraices.com/" style="font-weight: bold; color: #2A5DB0;">www.marazulbienesraices.com</a>
										</p>
									</td>
								</tr>
							</table>

							<br /><br />

							<table cellspacing="0" align="center" style="font-size: 12px; line-height: 20px; font-family: Arial, sans-serif; color: #fff;" width="560" cellpadding="0">
								<tr>
									<td class="logo" valign="top" width="160">
										<a href="http://www.marazulbienesraices.com/" style="color: #2A5DB0;">
											<img src="<?php echo $this->Html->url($imgDir.'email/logo.png', true); ?>" border="0" height="57" alt="" width="182" />
										</a>
									</td>
									<td  align="right" valign="middle" width="400">
										<p>
											<a href="http://www.marazulbienesraices.com/nosotros" style="color: #fff; text-decoration: underline;">Nosotros</a>
											| <a href="http://www.marazulbienesraices.com/rentas" style="color: #fff; text-decoration: underline;">Renta</a>
											| <a href="http://www.marazulbienesraices.com/ventas" style="color: #fff; text-decoration: underline;">Venta</a>
											| <a href="http://www.marazulbienesraices.com/contacto" style="color: #fff; text-decoration: underline;">Contacto</a>
										</p>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<!-- END HEADER CONTENT -->

				<br />

				<table rules="none" cellspacing="0" border="1" bordercolor="#d6d6d6" frame="border" align="center" style="font-size: 12px; border-color: #d6d6d6 #d6d6d6 #d6d6d6 #d6d6d6; border-collapse: collapse; font-family: Arial, sans-serif; line-height: 20px; color: #666666; border-spacing: 0px; border-width: 1px 1px 1px 1px; border-style: solid solid solid solid; background: #ffffff;" bgcolor="#ffffff" width="600" cellpadding="0">
					<tr>
						<td>
							<img src="<?php echo $this->Html->url($imgDir.'email/module-divider-3.gif', true); ?>" height="30" alt="" width="600" />

							<!-- START MAIN CONTENT. ADD MODULES BELOW -->

							<!-- Module #1 | 1 col, 540px -->
							<table cellspacing="0" align="center" style="font-size: 12px; line-height: 20px; font-family: Arial, sans-serif; color: #666666;" width="540" cellpadding="0">
								<tr>
									<td valign="top" width="400">
										<?php
											echo $this->Html->image($this->Html->url($imgDir.'images/main_img_'.$mainImages[0]['MainImage']['image'], true), array('style'=>'height: 200px; width: 400px'));
										?>
									</td>
									<td width="20">
										<img src="<?php echo $this->Html->url($imgDir.'email/spacer20x20.gif', true); ?>" height="20" alt="" width="20" />
									</td><!-- spacer column -->
									<td valign="top" width="120">
										<?php for($i=1; $i<4; $i++): ?>
										<p>
											<?php
												echo $this->Html->image($this->Html->url($imgDir.'images/main_img_'.$mainImages[$i]['MainImage']['image'], true), array('style'=>'height: 59px; width: 120px'));
											?>
										</p>
										<?php endfor; ?>
									</td>
								</tr>
							</table>
							<!-- End Module #1 -->

							<?php
								echo $this->Html->image($this->Html->url($imgDir.'email/module-divider.gif', true), array('style'=>'height: 61px; width: 600px'));
							?>

							<!-- Module #4 | 2 columns, 400px, 20px spacer, 120px  -->
							<table cellspacing="0" align="center" style="font-size: 12px; line-height: 20px; font-family: Arial, sans-serif; color: #666666;" width="540" cellpadding="0">
								<tr>
									<td valign="top">
										<h1 style="font-size: 22px; font-family: Arial, sans-serif; color: #000;">¡Novedades en Renta!</h1>
									</td>
								</tr>
							</table>

							<?php
								echo $this->Html->image($this->Html->url($imgDir.'email/module-divider-3.gif', true), array('style'=>'height: 30px; width: 600px'));
							?>

							<!-- Module #7 | 3 equal columns, 166px, 20px, 166px, 20px, 166px -->
							<table cellspacing="0" align="center" style="font-size: 12px; line-height: 20px; font-family: Arial, sans-serif; color: #666666;" width="540" cellpadding="0">
								<tr>
								<?php foreach($recentRents as $key => $rent): ?>
									<td valign="top" width="166">
										<?php
											echo $this->Html->image($this->Html->url($imgDir.'images/rents_img_'.$rent['Rent']['main_image'], true), array('style'=>'height: 135px; width: 166px; align: middle'));
										?>
										<!-- <img src="template-3/166x135.jpg" height="135" align="middle" width="166" /> -->

										<h4 style="font-size: 14px; font-family: Arial, sans-serif; color: #000;"><?php echo $rent['Rent']['type'].' en '.$rent['City']['name'].', '.$rent['State']['name']; ?></h4>

										<ul style="padding-left: 16px; margin-left: 0;">
											<li><?php echo $rent['Rent']['floors'].' planta'; echo $rent['Rent']['floors']>1 ? 's':''; ?></li>
											<li><?php echo $rent['Rent']['bedrooms'].' recámara'; echo $rent['Rent']['bedrooms']>1 ? 's':''; ?></li>
											<li><?php echo $rent['Rent']['bathrooms'].' baño'; echo $rent['Rent']['bathrooms']>1 ? 's':''; ?></li>
											<li><?php echo $rent['Rent']['terrain_size']; ?></li>
										</ul>

										<p>
											<a href="#" style="color: #2A5DB0;">
												VER
											</a>
										</p>
									</td>
									<?php if($key!=2): ?>
									<td width="20">
										<?php
											echo $this->Html->image($this->Html->url($imgDir.'email/spacer20x20.gif', true), array('style'=>'height: 20px; width: 20px'));
										?>
									</td><!-- spacer column -->
									<?php endif; ?>
								<?php endforeach; ?>
								</tr>
							</table>
							<!-- End Module #7 -->

							<?php
								echo $this->Html->image($this->Html->url($imgDir.'email/module-divider-3.gif', true), array('style'=>'height: 30px; width: 600px'));
							?>

							<!-- Module #4 | 2 columns, 400px, 20px spacer, 120px  -->
							<table cellspacing="0" align="center" style="font-size: 12px; line-height: 20px; font-family: Arial, sans-serif; color: #666666;" width="540" cellpadding="0">
								<tr>
									<td valign="top">
										<h1 style="font-size: 22px; font-family: Arial, sans-serif; color: #000;">¡Novedades en Venta!</h1>
									</td>
								</tr>
							</table>

							<?php
								echo $this->Html->image($this->Html->url($imgDir.'email/module-divider-3.gif', true), array('style'=>'height: 30px; width: 600px'));
							?>

							<!-- Module #7 | 3 equal columns, 166px, 20px, 166px, 20px, 166px -->
							<table cellspacing="0" align="center" style="font-size: 12px; line-height: 20px; font-family: Arial, sans-serif; color: #666666;" width="540" cellpadding="0">
								<tr>
								<?php foreach($recentSales as $key => $sale): ?>
									<td valign="top" width="166">
										<?php
											echo $this->Html->image($this->Html->url($imgDir.'images/sales_img_'.$sale['Sale']['main_image'], true), array('style'=>'height: 135px; width: 166px; align: middle'));
										?>
										<!-- <img src="template-3/166x135.jpg" height="135" align="middle" width="166" /> -->

										<h4 style="font-size: 14px; font-family: Arial, sans-serif; color: #000;"><?php echo $sale['Sale']['type'].' en '.$sale['City']['name'].', '.$sale['State']['name']; ?></h4>

										<ul style="padding-left: 16px; margin-left: 0;">
											<li><?php echo $sale['Sale']['floors'].' planta'; echo $sale['Sale']['floors']>1 ? 's':''; ?></li>
											<li><?php echo $sale['Sale']['bedrooms'].' recámara'; echo $sale['Sale']['bedrooms']>1 ? 's':''; ?></li>
											<li><?php echo $sale['Sale']['bathrooms'].' baño'; echo $sale['Sale']['bathrooms']>1 ? 's':''; ?></li>
											<li><?php echo $sale['Sale']['terrain_size']; ?></li>
										</ul>

										<p>
											<a href="#" style="color: #2A5DB0;">
												VER
											</a>
										</p>
									</td>
									<?php if($key!=2): ?>
									<td width="20">
										<?php
											echo $this->Html->image($this->Html->url($imgDir.'email/spacer20x20.gif', true), array('style'=>'height: 20px; width: 20px'));
										?>
									</td><!-- spacer column -->
									<?php endif; ?>
								<?php endforeach; ?>
								</tr>
							</table>
							<!-- End Module #7 -->

							<!-- END MAIN CONTENT -->
							<!-- START SOCIAL + COMPANY INFO + SUBSCRIPTION -->

							<?php
								echo $this->Html->image($this->Html->url($imgDir.'email/module-divider-3.gif', true), array('style'=>'height: 30px; width: 600px'));
							?>

							<table cellspacing="0" style="font-size: 12px; border-bottom: 1px solid #D0D0D0; font-family: Arial, sans-serif; line-height: 20px; color: #666666; border-top: 1px solid #D0D0D0;" bgcolor="#efefef" cellpadding="0">
								<tr>
									<td height="45" valign="middle" width="30">&nbsp;</td>
									<td height="45" valign="middle" width="185"><b style="color: #333333;">Conéctate con nosotros:</b></td>
									<td height="45" valign="middle" width="25">
										<a href="<?php echo !empty($confs['social-networks-facebook']) ? $confs['social-networks-facebook'] : ''; ?>" title="" style="color: #3a3a3a; text-decoration: none;">
											<?php
												echo $this->Html->image($this->Html->url($imgDir.'email/social/facebook.png', true), array('style'=>'height: 24px; width: 24px; border: 0'));
											?>
										</a>
									</td>
									<td height="45" valign="middle" width="165">
										&nbsp;&nbsp;
										<a href="<?php echo !empty($confs['social-networks-facebook']) ? $confs['social-networks-facebook'] : ''; ?>" title="" style="color: #3a3a3a; text-decoration: none;">Facebook</a>
									</td>
									<td height="45" valign="middle" width="25">
										<a href="<?php echo !empty($confs['social-networks-twitter']) ? $confs['social-networks-twitter'] : ''; ?>" title="" style="color: #3a3a3a; text-decoration: none;">
											<?php
												echo $this->Html->image($this->Html->url($imgDir.'email/social/twitter.png', true), array('style'=>'height: 24px; width: 24px; border: 0'));
											?>
										</a>
									</td>
									<td height="45" valign="middle" width="170">
										&nbsp;&nbsp;
										<a href="<?php echo !empty($confs['social-networks-twitter']) ? $confs['social-networks-twitter'] : ''; ?>" title="" style="color: #3a3a3a; text-decoration: none;">Twitter</a>
									</td>
								</tr>
							</table>

							<?php
								echo $this->Html->image($this->Html->url($imgDir.'email/module-divider-3.gif', true), array('style'=>'height: 30px; width: 600px'));
							?>

							<table cellspacing="0" align="center" style="font-size: 12px; line-height: 20px; font-family: Arial, sans-serif; color: #666666;" width="540" cellpadding="0">
								<tr>
									<td valign="top" width="265">
										<p>
											<b style="color: #333333;"><?php echo !empty($confs['website-title']) ? $confs['website-title'] : ''; ?></b>
											<br /><?php echo !empty($confs['contact-address']) ? $confs['contact-address'] : ''; ?>
											<br /><?php echo !empty($confs['contact-phone']) ? $confs['contact-phone'] : ''; ?>
											<br /><a href="mailto:<?php echo !empty($confs['contact-email']) ? $confs['contact-email'] : ''; ?>" style="color: #2A5DB0;"><?php echo !empty($confs['contact-email']) ? $confs['contact-email'] : ''; ?></a>
										</p>
									</td>

									<td width="20">
										<?php
											echo $this->Html->image($this->Html->url($imgDir.'email/spacer20x20.gif', true), array('style'=>'height: 20px; width: 20px'));
										?>
									</td><!-- spacer column -->
									<td align="left" valign="top" width="255">
										<p>
											Este boletín fue enviado a <?php echo $subscriberEmail; ?>
											<br />de <?php echo !empty($confs['website-title']) ? $confs['website-title'] : ''; ?>
											<br />ya que la dirección está suscrita al servicio.
											<br /><a href="http://www.marazulbienesraices.com/" style="color: #2A5DB0;">No deseo recibir este correo</a>
										</p>
									</td>
								</tr>
							</table>

							<?php
								echo $this->Html->image($this->Html->url($imgDir.'email/module-divider-3.gif', true), array('style'=>'height: 30px; width: 600px'));
							?>
							<!-- END SOCIAL + COMPANY INFO + SUBSCRIPTION -->
						</td>
					</tr>
				</table>

				<br />

				<table cellspacing="0" align="center" style="font-size: 12px; line-height: 20px; font-family: Arial, sans-serif; color: #666666;" width="560" cellpadding="0">
					<tr>
						<td align="center" valign="top">
							<!-- START FOOTER CONTENT -->
							<p>&copy; <?php echo date('Y') ?> <b style="color: #333333;"><?php echo !empty($confs['website-title']) ? $confs['website-title'] : ''; ?></b>. Todos los derechos reservados.</p>
							<!-- END FOOTER CONTENT -->
						</td>
					</tr>
				</table>
				<br />
				<!-- ///////////////////////////////////// End Newsletter Content  ///////////////////////////////////// -->
			</td>
		</tr>
	</table><!-- end wrapper table-->
</body>
</html>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="es"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
  <?php echo $this->Html->charset(); ?>
  <?php $title_for_layout = 'Panel de Administración' ?>
  <title><?php echo $title_for_layout; ?></title>
  <?php echo $this->Html->meta('author', '') ?>
  <?php echo $this->Html->meta('keywords', '') ?>
  <?php echo $this->Html->meta('description', '') ?>
  <?php echo $this->Html->meta('viewport', 'width=device-width, initial-scale=1.0') ?>

  <?php
    //echo $this->Html->meta('icon');

    //Template CSS's
    echo $this->Html->css(array(
      'admin/bootstrap/css/bootstrap.min',
      'admin/css/metro',
      'admin/font-awesome/css/font-awesome',
      'admin/css/style',
      'admin/css/style_responsive',
      'admin/css/style_default',
      'admin/css/style_default',
      'admin/uniform/css/uniform.default'
      ));

    echo $this->fetch('meta');
    echo $this->fetch('css');
    echo $this->fetch('script');
  ?>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="login">
  <!-- BEGIN LOGO -->
  <div class="logo">
    <?php echo $this->Html->image('admin/img/logo-big.png');?>
  </div>
  <!-- END LOGO -->
  <!-- BEGIN LOGIN -->
  <div class="content">
    <h3 class="form-title">Inicia Sesión</h3>
    <?php echo $this->Session->flash(); ?>
    <?php echo $this->Session->flash('auth', array('element' => 'admin/login_flash')); ?>
    <?php echo $this->fetch('content'); ?>
  </div>
  <!-- END LOGIN -->
  <!-- BEGIN COPYRIGHT -->
  <div class="copyright">
    <?php echo date('Y') ?> &copy;
  </div>
  <!-- END COPYRIGHT -->
  <!-- BEGIN JAVASCRIPTS -->
  <?php echo $this->Html->script(array(
          'admin/js/jquery-1.8.3.min.js',
          'admin/bootstrap/js/bootstrap.min.js',
          'admin/uniform/jquery.uniform.min.js',
          'admin/js/jquery.blockui.js',
          'admin/jquery-validation/dist/jquery.validate.min.js',
          'admin/js/app.js')
        );
  ?>
  <script>
    jQuery(document).ready(function() {
      App.initLogin();
    });
  </script>
  <!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
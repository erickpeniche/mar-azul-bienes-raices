<!DOCTYPE html>
<html lang="es">
<head>
	<title><?php echo Configure::read('App.configurations.website-title'); ?> | <?php echo isset($viewTitle) ? $viewTitle : ''; ?></title>
	<?php echo $this->Html->charset(); ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php
		echo $this->Html->meta('favicon.ico', '/img/portal/favicon.ico', array('type' => 'icon', 'rel' => 'icon'));

		//Template CSS's
		echo $this->Html->css('portal/reset.css', null, array('media'=>'screen'));
		echo $this->Html->css('portal/skeleton.css', null, array('media'=>'screen'));
		echo $this->Html->css('portal/superfish.css', null, array('media'=>'screen'));
		echo $this->Html->css('portal/style.css', null, array('media'=>'screen'));
		echo $this->Html->css('portal/slider.css', null, array('media'=>'screen'));
		echo $this->Html->css('portal/tabs.css', null, array('media'=>'screen'));

		//Template JavaScript's
		echo $this->Html->script(array(
			'portal/jquery-1.7.1.min',
			'portal/script',
			'portal/jquery.hoverIntent',
			'portal/superfish',
			'portal/jquery.responsivemenu',
			//'portal/jquery.twitter',
			'portal/slides.min.jquery',
			'portal/jquery.easing.1.3',
			'portal/tabs',
			'portal/forms',
			'portal/subscribe'
			)
		);

		echo $this->fetch('script');
		echo $this->Js->writeBuffer();

		echo $this->fetch('meta');
		echo $this->fetch('css');
	?>
	<meta content="<?php echo Configure::read('App.configurations.website-description'); ?>" name="description" />
	<meta name="keywords" content="<?php echo Configure::read('App.configurations.website-keywords'); ?>">
	<meta content="Erick Peniche Castro" name="author" />

	<!--[if lt IE 8]>
		<div style='clear: both; text-align:center; position: relative;'>
			<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
				<img src="http://storage.ie6countdown.com/assets/100/portal/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today.">
			</a>
		</div>
	<![endif]-->
	<!--[if lt IE 9]>
		<script src="js/html5.js"></script>
		<link rel="stylesheet" href="css/ie.css">
	<![endif]-->
</head>
<body id="<?php echo isset($home) ? 'page1' : ''; ?>">
	<div class="bg">
		<div class="light">
			<!--======================== header ============================-->
			<header>
				<div class="container_12 <?php echo isset($home) ? 'indent-bottom' : ''; ?>">
					<div class="grid_12">
						<!--======================== logo ============================-->
						<h1><?php echo $this->Html->link('Mar Azul Bienes Raíces', array('controller'=>'pages', 'action'=>'home')); ?></h1>
						<!--======================== menu ============================-->
						<?php echo $this->element('portal/navigation'); ?>
						<div class="clear"></div>
					</div>
					<div class="clear"></div>
				</div>
				<?php echo $this->fetch('slider'); ?>
			</header>
			<!--======================== content ===========================-->
			<section id="content">
				<div class="container_12">
					<div class="wrapper">
						<div class="grid_12">
							<?php echo $this->Session->flash(); ?>
							<?php echo $this->fetch('content'); ?>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
	<!--======================== footer ============================-->
	<?php echo $this->element('portal/footer'); ?>
</body>
</html>
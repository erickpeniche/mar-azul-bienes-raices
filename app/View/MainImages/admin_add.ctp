<?php
   echo $this->Html->css(array(
	  'admin/bootstrap-fileupload/bootstrap-fileupload',
	  'admin/gritter/css/jquery.gritter',
	  'admin/jquery-tags-input/jquery.tagsinput',
	  'admin/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons',
	  'admin/data-tables/DT_bootstrap',
	  ), null, array('inline' => false)
   );

   echo $this->Html->script(array(
	  'admin/bootstrap-fileupload/bootstrap-fileupload',
	  'admin/jquery-tags-input/jquery.tagsinput.min',
	  'admin/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons'
	  ), array('inline' => false)
   );

   $this->set('activeMenu', 'mainImages');

   $this->Html->addCrumb('Imagenes Principales',
	  array(
		 'action' => 'index',
		 'controller' => 'main_images',
		 'admin' => true
		 )
	  );

   $this->Html->addCrumb('Nueva',
	  array(
		 'action' => 'add',
		 'controller' => 'main_images',
		 'admin' => true
		 )
	  );
?>
<div class="portlet box blue">
   <div class="portlet-title">
	  <h4><i class="icon-picture"></i>Nueva imagen</h4>
	  <div class="actions">
		 <?php echo $this->Html->link('<i class="icon-picture"></i> Imágenes Principales</a>',
			array(
			   'action' => 'index',
			   'controller' => 'main_images',
			   'admin' => true
			   ),
			array(
			   'class' => 'btn yellow',
			   'escape' => false)
			);
		 ?>
		 <!-- <a href="#" class="btn green"> -->
	  </div>
   </div>
	<div class="portlet-body form">
	<!-- BEGIN FORM-->
	<?php echo $this->Form->create('MainImage',
		array(
			'inputDefaults' => array(
				'div' => false,
				'class' => 'm-wrap span8'
				),
			'class' => 'form-horizontal form-row-seperated',
			'enctype' => 'multipart/form-data',
			'novalidate' => true
			)
		);
	?>

		<div class="control-group <?php echo isset($this->validationErrors['MainImage']['image']) ? 'error' : ''; ?>">
			<label class="control-label">Imagen<span class="required">*</span></label>
			<div class="controls">
				<div class="fileupload fileupload-new" data-provides="fileupload">
					<div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
						<?php echo $this->Html->image('http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=sin+imagen'); ?>
					</div>
					<div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
					<div>
						<span class="btn btn-file">
							<span class="fileupload-new">Seleccionar</span>
							<span class="fileupload-exists">Cambiar</span>
							<?php echo $this->Form->file('image', array('class' => 'default')); ?>
						</span>
						<a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Eliminar</a>
					</div>
				</div>
				<span class="label label-important">NOTA!</span>
				<span>La imagen debe pesar menos de <strong>20 MB</strong> y debe ser formato <strong>JPG, PNG o BMP</strong></span>
			</div>
		</div>

	  <?php
		 echo '<div class="form-actions">';
		 echo $this->Form->button('<i class="icon-ok"></i> Guardar',
			array('type' => 'submit', 'class' => 'btn green')
			);
		 echo '&nbsp;';

		 $cancelOptions = array('action' => 'index', 'controller' => 'main_images', 'admin' => true);
		 echo $this->Form->button('Cancelar',
			array(
			   'type' => 'button',
			   'class' => 'btn red',
			   'onclick' => "location.href='".$this->Html->url($cancelOptions)."'"
			   )
			);
		 echo '</div>';
	  ?>

	  <?php echo $this->Form->end(); ?>
	  <!-- END FORM-->
   </div>
</div>
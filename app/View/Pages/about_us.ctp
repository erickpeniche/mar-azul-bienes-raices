<div class="wrapper">
	<div class="grid_9 alpha">
		<div class="indent-bottom11 border-bottom p5">
			<h3 class="p3">Servicios</h3>
			<div class="wrapper">
				<div class="grid_4 alpha">
					<ul class="list-1">
						<li>Renta</li>
						<li class="last-item">Venta</li>
					</ul>
				</div>
				<div class="grid_4 omega">
					<ul class="list-1">
						<li>Administración de bienes inmuebles</li>
						<li class="last-item">Asesoría jurídica respecto a los servicios</li>
					</ul>
				</div>
			</div>
		</div>
		<h3 class="p5">Objetivo</h3>
		<div class="wrapper indent-bottom11 border-bottom p5">
			<figure class="img-indent5">
				<?php echo $this->Html->image('portal/page4-icon1.png', array('class'=>'icon')); ?>
			</figure>
			<div class="extra-wrap">
				Realizar los servicios a corto plazo, con los mejores precios del mercado.
			</div>
		</div>
		<h3 class="p5">Valores</h3>
		<div class="wrapper indent-bottom11 border-bottom p5">
			<div class="grid_3 alpha">
				<figure class="img-indent5">
					<?php echo $this->Html->image('portal/page2-icon1.png', array('class'=>'icon')); ?>
				</figure>
				<div class="extra-wrap">
					Ética profesional
				</div>
			</div>
			<div class="grid_3">
				<figure class="img-indent5">
					<?php echo $this->Html->image('portal/page6-icon2.png', array('class'=>'icon')); ?>
				</figure>
				<div class="extra-wrap">
					Honestidad
				</div>
			</div>
			<div class="grid_3 omega">
				<figure class="img-indent5">
					<?php echo $this->Html->image('portal/page4-icon3.png', array('class'=>'icon')); ?>
				</figure>
				<div class="extra-wrap">
					Seriedad
				</div>
			</div>
		</div>
		<h3 class="p5">Compromiso</h3>
		<div class="wrapper">
			<figure class="img-indent5">
				<?php echo $this->Html->image('portal/page4-icon4.png', array('class'=>'icon')); ?>
			</figure>
			<div class="extra-wrap">
				Otorgar un servicio de calidad, a la brevedad posible, y con los mejores precios.
			</div>
		</div>
	</div>
	<?php echo $this->element('portal/best-properties', array('bestRents' => $bestRents , 'bestSale' => $bestSale)); ?>
</div>
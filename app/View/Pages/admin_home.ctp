<?php
	echo $this->Html->css(array('admin/gritter/css/jquery.gritter'), null, array('inline' => false));

	$this->set('activeMenu', 'home');
?>
<!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
	<?php
		echo $this->element('admin/dashboard/dashboard_stat',
			array(
				'color' => 'yellow',
				'icon' => 'picture',
				'number' => $totalMainImages,
				'title' => 'Imágenes principales',
				'ctrlr' => 'main_images'
				)
			);
	?>
	<?php
		echo $this->element('admin/dashboard/dashboard_stat',
			array(
				'color' => 'purple',
				'icon' => 'tag',
				'number' => $totalRents,
				'title' => 'Rentas',
				'ctrlr' => 'rents'
				)
			);
	?>
	<?php
		echo $this->element('admin/dashboard/dashboard_stat',
			array(
				'color' => 'green',
				'icon' => 'money',
				'number' => $totalSales,
				'title' => 'Ventas',
				'ctrlr' => 'sales'
				)
			);
	?>
	<?php
		echo $this->element('admin/dashboard/dashboard_stat',
			array(
				'color' => 'red',
				'icon' => 'envelope',
				'number' => $totalSubscribers,
				'title' => 'Suscriptores y boletín',
				'ctrlr' => 'subscribers'
				)
			);
	?>
</div>
<div class="row-fluid">
	<?php
		echo $isSuperUser ? $this->element('admin/dashboard/dashboard_stat',
			array(
				'color' => 'blue',
				'icon' => 'group',
				'number' => $totalUsers,
				'title' => 'Usuarios',
				'ctrlr' => 'users'
				)
			) : '';
	?>
	<?php
		echo $isSuperUser ? $this->element('admin/dashboard/dashboard_stat',
			array(
				'color' => 'bg-grey',
				'icon' => 'cogs',
				'number' => '',
				'title' => 'Configuraciones',
				'ctrlr' => 'configurations'
				)
			) : '';
	?>
</div>
<!-- END PAGE CONTENT-->
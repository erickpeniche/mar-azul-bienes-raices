<?php
	echo $this->Html->css(array('portal/forms'), null, array('media'=>'screen', 'inline' => false));
	echo $this->Html->script(array('portal/contact', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDFMUFMx5D3Cv41yLkhycfCzvxpi1i7B1Y&sensor=false'),array('inline' => false));
?>
<div class="wrapper">
	<div class="grid_5 alpha">
		<h3 class="p4">Información de contacto</h3>
		<!--=============== Map ====================-->
		<div id="map">
			<?php if(isset($confs['contact-map']) && !empty($confs['contact-map'])): ?>
			<figure class="p5">
				<div id="gmap" data-coordinates="<?php echo $confs['contact-map']; ?>"></div>
			</figure>
			<?php else: ?>
			<figure class="p5">
				<iframe src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Brooklyn,+New+York,+NY,+United+States&amp;aq=0&amp;sll=37.0625,-95.677068&amp;sspn=61.282355,146.513672&amp;ie=UTF8&amp;hq=&amp;hnear=Brooklyn,+Kings,+New+York&amp;ll=40.649974,-73.950005&amp;spn=0.01628,0.025663&amp;z=14&amp;iwloc=A&amp;output=embed"></iframe>
			</figure>
			<?php endif; ?>
			<dl>
				<dt><?php echo isset($confs['contact-address']) && !empty($confs['contact-address']) ? $confs['contact-address'] : '<em>No hay dirección</em>';?>
				</dt>
				<dd><span>Teléfono:</span> <?php echo isset($confs['contact-phone']) && !empty($confs['contact-phone']) ? $confs['contact-phone'] : '<em>No hay teléfono</em>';?></dd>
				<dd>E-mail: <?php echo isset($confs['contact-email']) && !empty($confs['contact-email']) ? $this->Text->autoLinkEmails($confs['contact-email']) : '<em>No hay email</em>';?></dd>
			</dl>
		</div>
	</div>
	<div class="grid_7 omega">
		<h3 class="p4">Escríbenos</h3>
		<!--=========== Contact form ==============-->
		<form id="contact-form">
			<div class="success">
				Mensaje enviado!<br>
				<strong>Nos pondremos en contacto pronto</strong>
			</div>
			<fieldset>
				<?php
					echo $this->Form->create('Contact', array('inputDefaults' => array('div' => false, 'label'=>false),'novalidate' => true, 'id'=>'contact-form'));
				?>
				<label class="name">
					<?php echo $this->Form->input('name', array('value'=>'Nombre:')); ?>
					<span class="error">*No es un nombre válido.</span> <span class="empty">*Escribe un nombre.</span>
				</label>
				<label class="email">
					<?php echo $this->Form->input('email', array('value'=>'Email:')); ?>
					<span class="error">*No es una dirección de correo electrónico válida.</span> <span class="empty">*Escribe una dirección de correo electrónico.</span>
				</label>
				<label class="phone">
					<?php echo $this->Form->input('phone', array('value'=>'Teléfono:')); ?>
					<span class="error">*No es un teléfono válido.</span> <span class="empty">*Escribe un teléfono.</span>
				</label>
				<label class="message" name="message">
					<?php echo $this->Form->textarea('message', array('value'=>'Mensaje:')); ?>
					<span class="error">*El mensaje está muy corto.</span> <span class="empty">*Escribe un mensaje.</span>
				</label>
				<div class="buttons-wrapper">
					<?php echo $this->Html->link('Enviar', '#', array('class'=>'button contact-send', 'data-type'=>'submit', 'data-form'=>'contact')); ?>
				</div>
				<?php echo $this->Form->end(); ?>
			</fieldset>
		</form>
	</div>
</div>
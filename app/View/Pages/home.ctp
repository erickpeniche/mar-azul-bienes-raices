<?php
	$this->append('slider');
	echo $this->element('portal/slider');
	$this->end();
?>
<!--================== Site Values ==================-->
<div class="wrapper indent-bottom11 border-bottom p7">
	<div class="grid_4 alpha">
		<div class="box1 icon-1">
			<h3 class="p3"><?php echo $this->Html->link('Rentas', array('controller'=>'rents', 'action'=>'index')); ?></h3>
			Lorem ipsum dolor sit amet, cosectur adipiscing elit. Aenean vel orci quis risus facilisis vehicula. Pellentesque purus libero, ullamcorper a aliquet
		</div>
	</div>
	<div class="grid_4">
		<div class="box1 icon-2">
			<h3 class="p3"><?php echo $this->Html->link('Ventas', array('controller'=>'sales', 'action'=>'index')); ?></h3>
			Lorem ipsum dolor sit amet, cosectur adipiscing elit. Aenean vel orci quis risus facilisis vehicula. Pellentesque purus libero, ullamcorper a aliquet
		</div>
	</div>
	<div class="grid_4 omega">
		<div class="box1 icon-3">
			<h3 class="p3"><a>Administración</a></h3>
			Ofrecemos administración de bienes inmuebles y asesoría jurídica para satisfacer las necesidades de todos nuestros clientes
		</div>
	</div>
</div>
<!--================== END Site Values ==================-->

<!--================== Rent Properties grid ==================-->
<div class="wrapper indent-bottom11 border-bottom p10-1">
	<h3 class="p3">Propiedades en Renta</h3>
	<div class="tabs">
		<ul class="nav">
			<li class="first-item"><a href="#tab1">Recientes</a></li> <!-- All recent properties for rent-->
			<li class="last-item"><a href="#tab2">Destacadas</a></li> <!-- All best offer properties for rent-->
		</ul>
		<div class="tab_container">
			<?php echo $this->element('portal/properties_grid_tab', array('data' => $recentRents , 'tabId' => 'tab1', 'imgPrefix' => 'rents', 'model' => 'Rent')); ?>
			<?php echo $this->element('portal/properties_grid_tab', array('data' => $bestRents , 'tabId' => 'tab2', 'imgPrefix' => 'rents', 'model' => 'Rent')); ?>
		</div>
	</div>
</div>
<!--================== Sale Properties grid ==================-->
<div class="wrapper">
	<h3 class="p3">Propiedades en Venta</h3>
	<div class="tabs">
		<ul class="nav">
			<li class="first-item"><a href="#tab3">Recientes</a></li> <!-- All recent properties for sale-->
			<li class="item"><a href="#tab4">Destacadas</a></li> <!-- All best offer properties for sale-->
			<li class="last-item"><a href="#tab5">Ofertas</a></li> <!-- All discount available properties for sale-->
		</ul>
		<div class="tab_container">
			<?php echo $this->element('portal/properties_grid_tab', array('data' => $recentSales , 'tabId' => 'tab3', 'imgPrefix' => 'sales', 'model' => 'Sale')); ?>
			<?php echo $this->element('portal/properties_grid_tab', array('data' => $bestSales , 'tabId' => 'tab4', 'imgPrefix' => 'sales', 'model' => 'Sale')); ?>
			<?php echo $this->element('portal/properties_grid_tab', array('data' => $onSaleSales , 'tabId' => 'tab5', 'imgPrefix' => 'sales', 'model' => 'Sale')); ?>
		</div>
	</div>
</div>
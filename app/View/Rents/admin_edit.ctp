<?php
	echo $this->Html->css(array(
		'admin/bootstrap-fileupload/bootstrap-fileupload',
		'admin/gritter/css/jquery.gritter',
		'admin/jquery-tags-input/jquery.tagsinput',
		'admin/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons',
		'admin/data-tables/DT_bootstrap',
		'admin/bootstrap-wysihtml5/bootstrap-wysihtml5'
		), null, array('inline' => false)
	);

	echo $this->Html->script(array(
		'admin/bootstrap-fileupload/bootstrap-fileupload',
		'admin/jquery-tags-input/jquery.tagsinput.min',
		'admin/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons',
		'admin/jquery-validation/dist/jquery.validate.min',
		'admin/jquery-validation/dist/additional-methods.min',
		'admin/ckeditor/ckeditor',
		'admin/bootstrap-wysihtml5/wysihtml5-0.3.0',
		'admin/bootstrap-wysihtml5/bootstrap-wysihtml5',
		'admin/rent_state_city',
		'http://maps.google.com/maps/api/js?sensor=false',
		'admin/js/gmaps',
		'admin/property-map'
		), array('inline' => false)
	);

	$this->set('activeMenu', 'rents');

	$this->Html->addCrumb('Rentas',
		array(
			'action' => 'index',
			'controller' => 'rents',
			'admin' => true
			)
		);

	$this->Html->addCrumb('Editar', null);
?>
<div class="portlet box blue">
	<div class="portlet-title">
		<h4><i class="icon-tag"></i>Editar renta</h4>
		<div class="actions">
			<?php echo $this->Html->link('<i class="icon-tag"></i> Rentas</a>',
				array(
					'action' => 'index',
					'controller' => 'rents',
					'admin' => true
					),
				array(
					'class' => 'btn purple',
					'escape' => false)
				);
			?>
		</div>
	</div>
	<div class="portlet-body form">
		<!-- BEGIN FORM-->
		<?php echo $this->Form->create('Rent',
			array(
				'inputDefaults' => array(
					'div' => false,
					'class' => 'm-wrap span8'
					),
				'class' => 'form-horizontal form-row-seperated',
				'enctype' => 'multipart/form-data',
				'novalidate' => true
				)
			);
			echo $this->Form->input('id', array('type'=>'hidden'));
		?>

		<div class="control-group">
			<?php
				$types = array('Casa' => 'Casa', 'Departamento' => 'Departamento');
				echo $this->Form->input('type',
					array(
						'between' => '<div class="controls input-icon">',
						'options' => $types,
						'default' => 'Casa',
						'class'=>'m-wrap span3',
						'label' => array('text'=>'Tipo de inmueble', 'class'=>'control-label')
						)
					);
				echo '</div>';
			?>
		</div>

		<div class="control-group <?php echo isset($this->validationErrors['Rent']['state_id']) ? 'error' : ''; ?>">
			<?php
				echo $this->Form->input('state_id',
					array(
						'between' => '<div class="controls input-icon">',
						'options' => $states,
						'empty' => '(Seleccione un Estado)',
						'class'=>'m-wrap span3 chosen-with-diselect',
						'label' => array('text'=>'Estado', 'class'=>'control-label')
						)
					);
				echo isset($this->validationErrors['Rent']['state_id']) ? '<span class="help-inline">&nbsp;Seleccione un Estado</span></div>' : '</div>';
			?>
		</div>

		<?php $cityId = isset($this->data['City']['id']) && !empty($this->data['City']['id']) ? $this->data['City']['id'] : ''; ?>
		<div class="control-group" id="city-dropdown" data-city="<?php echo $cityId; ?>">
			<?php
				echo $this->Form->input('city_id',
					array(
						'between' => '<div class="controls input-icon">',
						'type' => 'select',
						'empty' => '(Primero seleccione estado)',
						'class'=>'m-wrap span3 chosen-with-diselect',
						'label' => array('text'=>'Ciudad', 'class'=>'control-label')
						)
					);
				echo '</div>';
			?>
		</div>

		<div class="control-group <?php echo isset($this->validationErrors['Rent']['address']) ? 'error' : ''; ?>">
			<?php
				echo $this->Form->input('address',
					array(
						'between' => '<div class="controls input-icon">',
						'label' => array('text'=>'Dirección<span class="required">*</span>', 'class'=>'control-label'),
						'error' => array(
							'attributes' => array('escape' => false, 'wrap' => false)
							)
						)
					);
				echo '<span class="help-block"><em>Escribe la dirección o ubicación del inmueble</em></span></div>';
			?>
		</div>

		<div class="control-group <?php echo isset($this->validationErrors['Rent']['terrain_size']) ? 'error' : ''; ?>">
			<?php
				echo $this->Form->input('terrain_size',
					array(
						'between' => '<div class="controls input-icon">',
						'label' => array('text'=>'Tamaño del terreno<span class="required">*</span>', 'class'=>'control-label'),
						'error' => array(
							'attributes' => array('escape' => false, 'wrap' => false)
							)
						)
					);
				echo '<span class="help-block"><em>Escribe el tamaño del terreno, ej. 100 metros cuadrados, 75 m2</em></span></div>';
			?>
		</div>

		<div class="control-group">
			<?php
				$floors = array('1' => '1', '2' => '2', '3' => '3', '4' => '4', '0' => '5+');
				echo $this->Form->input('floors',
					array(
						'between' => '<div class="controls input-icon">',
						'options' => $floors,
						'default' => '1',
						'class'=>'m-wrap span3',
						'label' => array('text'=>'Pisos/Plantas<span class="required">*</span>', 'class'=>'control-label')
						)
					);
				echo '</div>';
			?>
		</div>

		<div class="control-group">
			<?php
				$bedrooms = array('1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8' => '8', '9' => '9', '0' => '10+');
				echo $this->Form->input('bedrooms',
					array(
						'between' => '<div class="controls input-icon">',
						'options' => $bedrooms,
						'default' => '1',
						'class'=>'m-wrap span3',
						'label' => array('text'=>'Recámaras/Cuartos<span class="required">*</span>', 'class'=>'control-label')
						)
					);
				echo '</div>';
			?>
		</div>

		<div class="control-group">
			<?php
				$bathrooms = array('1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8' => '8', '9' => '9', '0' => '10+');
				echo $this->Form->input('bathrooms',
					array(
						'between' => '<div class="controls input-icon">',
						'options' => $bathrooms,
						'default' => '1',
						'class'=>'m-wrap span3',
						'label' => array('text'=>'Baños<span class="required">*</span>', 'class'=>'control-label')
						)
					);
				echo '</div>';
			?>
		</div>

		<div class="control-group <?php echo isset($this->validationErrors['Rent']['cost']) ? 'error' : ''; ?>">
			<label class="control-label">Costo de Renta<span class="required">*</span></label>
			<div class="controls">
				<div class="input-prepend input-append">
					<span class="add-on">$</span><?php echo $this->Form->input('cost', array('class'=>'m-wrap', 'label'=>false, 'type'=>'text', 'error' => false, 'style' => 'text-align: right')); ?><span class="add-on">.00</span>
				</div>
			</div>
		</div>

		<div class="control-group <?php echo isset($this->validationErrors['Rent']['contract_time']) ? 'error' : ''; ?>">
			<?php
				echo $this->Form->input('contract_time',
					array(
						'between' => '<div class="controls input-icon">',
						'label' => array('text'=>'Duración del contrato<span class="required">*</span>', 'class'=>'control-label'),
						'error' => array(
							'attributes' => array('escape' => false, 'wrap' => false)
							)
						)
					);
				echo '<span class="help-block"><em>Escribe la duración del contrato, ej. 1 año</em></span></div>';
			?>
		</div>

		<div class="control-group <?php echo isset($this->validationErrors['Rent']['description']) ? 'error' : ''; ?>">
			<?php
				echo $this->Form->input('description',
					array(
						'type'=>'textarea',
						'class'=>'span12 ckeditor m-wrap',
						'rows'=>6,
						'between' => '<div class="controls input-icon">',
						'label' => array('text'=>'Descripción<span class="required">*</span>', 'class'=>'control-label'),
						'error' => array(
							'attributes' => array('escape' => false, 'wrap' => false)
							)
						)
					);
				echo '</div>';
			?>
		</div>

		<div class="control-group <?php echo isset($this->validationErrors['Rent']['requirements']) ? 'error' : ''; ?>">
			<?php
				echo $this->Form->input('requirements',
					array(
						'type'=>'textarea',
						'class'=>'span12 ckeditor m-wrap',
						'rows'=>6,
						'between' => '<div class="controls input-icon">',
						'label' => array('text'=>'Requerimientos<span class="required">*</span>', 'class'=>'control-label'),
						'error' => array(
							'attributes' => array('escape' => false, 'wrap' => false)
							)
						)
					);
				echo '</div>';
			?>
		</div>

		<div class="control-group required">
			<label for="ConfigurationValue" class="control-label">Ubicación en el mapa</label>
			<div class="controls input-icon">
				<div class="label label-important visible-ie8">Not supported in Internet Explorer 8</div>
				<div id="gmap_marker" class="gmaps" data-model="Rent"></div>
				<div id="map-info">
					<?php
						echo $this->Form->button('<i class="icon-undo"></i> Limpiar',
							array('type' => 'button', 'class' => 'btn', 'id'=>'clean-map')
						);
					?>
				</div>
			</div>
		</div>

		<?php
			echo $this->Form->input('latitude', array('type'=>'hidden'));
			echo $this->Form->input('longitude', array('type'=>'hidden'));
		?>

		<div class="control-group <?php echo isset($this->validationErrors['Rent']['main_image']) ? 'error' : ''; ?>">
			<label class="control-label">Foto principal<span class="required">*</span></label>
			<div class="controls">
				<div class="fileupload fileupload-new" data-provides="fileupload">
					<div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
						<?php
							$img = !empty($this->data['Rent']['main_image']) ? 'images/rents_img_edt_'.$this->data['Rent']['main_image'] : 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=sin+imagen';
							echo $this->Html->image($img);
						?>
					</div>
					<div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
					<div>
						<span class="btn btn-file">
							<span class="fileupload-new">Seleccionar</span>
							<span class="fileupload-exists">Cambiar</span>
							<?php echo $this->Form->file('main_image', array('class' => 'default')); ?>
						</span>
						<a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Eliminar</a>
					</div>
				</div>
				<span class="help-block"><em>Es la imagen que aparecerá en la portada del anuncio en la página principal</em></span>
				<span class="label label-important">NOTA!</span>
				<span>La imagen debe pesar menos de <strong>20 MB</strong> y debe ser formato <strong>JPG, PNG o BMP</strong></span>
			</div>
		</div>

		<div class="control-group required">
			<label for="RentActive" class="control-label">Activo</label>
			<div class="controls">
				<div class="success-toggle-button">
					<?php
						$checked = $this->data['Rent']['active'] == 1 ? true : false;
						echo $this->Form->input('active',
							array(
								'type'=>'checkbox',
								'label'=>false,
								'id'=>'RentActive',
								'class'=>'toggle',
								'checked'=>$checked
							)
						);
					?>
				</div>
				<span class="help-block"><em>SÍ para mostrar el inmueble en los catálogos, NO para ocultarlo.</em></span>
			</div>
		</div>

		<div class="control-group required">
			<label for="RentActive" class="control-label">¿Se encuentra rentado?</label>
			<div class="controls">
				<div class="success-toggle-button">
					<?php
						$checked = $this->data['Rent']['rented'] == 1 ? true : false;
						echo $this->Form->input('best_offer',
							array(
								'type'=>'checkbox',
								'label'=>false,
								'id'=>'RentActive',
								'class'=>'toggle',
								'checked'=>$checked
							)
						);
					?>
				</div>
				<span class="help-block"><em>SÍ para marcar el inmueble como RENTADO.</em></span>
			</div>
		</div>

		<div class="control-group required">
			<label for="RentActive" class="control-label">Oferta destacada</label>
			<div class="controls">
				<div class="success-toggle-button">
					<?php
						$checked = $this->data['Rent']['best_offer'] == 1 ? true : false;
						echo $this->Form->input('best_offer',
							array(
								'type'=>'checkbox',
								'label'=>false,
								'id'=>'RentActive',
								'class'=>'toggle',
								'checked'=>$checked
							)
						);
					?>
				</div>
				<span class="help-block"><em>SÍ para mostrar el inmueble como oferta destacada en la página principal.</em></span>
			</div>
		</div>

		<div class="form-actions">
			<?php
				echo $this->Form->button('<i class="icon-ok"></i> Guardar',
					array('type' => 'submit', 'class' => 'btn green')
					);
				echo '&nbsp;';

				$cancelOptions = array('action' => 'index', 'controller' => 'rents', 'admin' => true);
				echo $this->Form->button('Cancelar',
					array(
						'type' => 'button',
						'class' => 'btn red',
						'onclick' => "location.href='".$this->Html->url($cancelOptions)."'"
						)
					);
			?>
		</div>
		<?php echo $this->Form->end(); ?>
		<!-- END FORM-->
	</div>
</div>
<?php
	echo $this->Html->css(array(
		'admin/fancybox/source/jquery.fancybox',
		'admin/data-tables/DT_bootstrap',
		), null, array('inline' => false)
	);

	$this->set('activeMenu', 'rents');

	$this->Html->addCrumb('Rentas',
		array(
			'action' => 'index',
			'controller' => 'rents',
			'admin' => true
			)
		);
?>
<!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
	<div class="span12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet box light-grey">
			<div class="portlet-title">
				<h4><i class="icon-tag"></i>Rentas</h4>
				<div class="actions">
					<?php echo $this->Html->link('<i class="icon-plus"></i> Nueva</a>',
						array(
							'action' => 'add',
							'controller' => 'rents'
							),
						array(
							'class' => 'btn purple',
							'escape' => false)
						);
					?>
				</div>
			</div>
			<div class="portlet-body">
				<table class="table table-striped table-bordered table-hover" id="sample_3">
					<thead>
						<tr>
							<th width='10%'>Foto</th>
							<th width='10%'><?php echo $this->Paginator->sort('State.name', 'Estado'); ?></th>
							<th width='10%'><?php echo $this->Paginator->sort('City.name', 'Ciudad'); ?></th>
							<th width='7%'><?php echo $this->Paginator->sort('type', 'Tipo'); ?></th>
							<th width='5%'><?php echo $this->Paginator->sort('floors', 'Plantas'); ?></th>
							<th width='9%'><?php echo $this->Paginator->sort('cost', 'Costo de Renta'); ?></th>
							<th width='5%'><?php echo $this->Paginator->sort('rented', 'Estado'); ?></th>
							<th width='44%'class="actions" width=''><?php echo __('Acciones'); ?></th>
						</tr>
					</thead>
					<tbody>
						<?php if (empty($rents)) : ?>
						<tr>
							<td colspan="8">
								<div class="alert alert-block alert-info fade in">
									<button type="button" class="close" data-dismiss="alert"></button>
									<h4 class="alert-heading">No hay propiedades en renta!</h4>
									<p>
										Aún no se han agregado propiedades en renta. Haz click en el botón 'Nueva' para agregar una nueva propiedad en renta.
									</p>
								</div>
							</td>
						</tr>
						<?php else: ?>
						<?php foreach ($rents as $rent): ?>
						<?php $img = isset($rent['Rent']['main_image']) && !empty($rent['Rent']['main_image']) ? '/img/images/rents_tbl_prev_'.$rent['Rent']['main_image'] : 'http://www.placehold.it/100x100/EFEFEF/AAAAAA&amp;text=Sin+foto' ?>
						<tr class="odd gradeX">
							<td><?php echo $this->Html->link($this->Html->image($img), array('action'=>'view', $rent['Rent']['id'], 'admin'=>true), array('escape'=>false)); ?></td>
							<td><?php echo h($rent['State']['name']); ?>&nbsp;</td>
							<td><?php echo h($rent['City']['name']); ?>&nbsp;</td>
							<td><?php echo h($rent['Rent']['type']); ?>&nbsp;</td>
							<td><?php echo h($rent['Rent']['floors']); ?>&nbsp;</td>
							<td><?php echo $this->Number->currency(h($rent['Rent']['cost']), 'USD'); ?>&nbsp;</td>
							<td>
								<?php if($rent['Rent']['rented']): ?>
								<span class="label label-success">Rentada</span>
								<?php else: ?>
								<span class="label label-important">Sin rentar</span>
								<?php endif; ?>
							</td>
							<td class="actions">
								<?php
									$stripColor = $rent['Rent']['active'] ? 'green' : 'grey';
									echo $this->Html->link('<i class="icon-eye-open"></i> Visible', array('action' => 'toggle_active', $rent['Rent']['id'], 'admin'=>true), array('class'=>'btn mini '.$stripColor.'-stripe', 'escape'=>false));
								?>
								<?php
									$stripColor = $rent['Rent']['rented'] ? 'green' : 'grey';
									echo $this->Html->link('<i class="icon-legal"></i> Rentada', array('action' => 'toggle_rented', $rent['Rent']['id'], 'admin'=>true), array('class'=>'btn mini '.$stripColor.'-stripe', 'escape'=>false));
								?>
								<?php
									$stripColor = $rent['Rent']['best_offer'] ? 'green' : 'grey';
									echo $this->Html->link('<i class="icon-certificate"></i> Oferta', array('action' => 'toggle_best_offer', $rent['Rent']['id'], 'admin'=>true), array('class'=>'btn mini '.$stripColor.'-stripe', 'escape'=>false));
								?>
								&nbsp;|&nbsp;
								<?php echo $this->Html->link('<i class="icon-info-sign"></i> Ver', array('action' => 'view', $rent['Rent']['id']), array('class'=>'btn mini blue-stripe', 'escape'=>false)); ?>
								<?php echo $this->Html->link('<i class="icon-edit"></i> Editar', array('action' => 'edit', $rent['Rent']['id']), array('class'=>'btn mini orange-stripe', 'escape'=>false)); ?>
								<?php echo $this->Form->postLink('<i class="icon-trash"></i> Eliminar', array('action' => 'delete', $rent['Rent']['id']), array('class'=>'btn mini red-stripe', 'escape'=>false), __('Confirma que desea eliminar esta propiedad en renta?')); ?>
							</td>
						</tr>
						<?php endforeach; ?>
						<?php endif; ?>
					</tbody>
				</table>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
<!-- END PAGE CONTENT-->

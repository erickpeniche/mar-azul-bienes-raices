<?php
	echo $this->Html->css(
		array(
			'admin/fancybox/source/jquery.fancybox',
			'admin/data-tables/DT_bootstrap'
			),
		null,
		array('inline' => false)
	);

	echo $this->Html->script(array('admin/jquery-slimscroll/jquery.slimscroll.min.js'), array('inline' => false));

	if(!empty($rent['Rent']['latitude']) && !empty($rent['Rent']['longitude'])){
		echo $this->Html->script(
			array(
				'http://maps.google.com/maps/api/js?sensor=true',
				'admin/js/gmaps.js',
				'admin/js/demo.gmaps.js'
			),
			array('inline' => false)
		);

		echo $this->Html->scriptBlock(
		    'jQuery(document).ready(function() {
				DemoGMaps.init();
			});',
		    array('inline' => false)
		);
	}


	$this->Html->addCrumb('Rentas',
		array(
			'action' => 'index',
			'controller' => 'rents',
			'admin' => true
			)
		);

	$this->set('activeMenu', 'rents');

	$this->Html->addCrumb('Ver', null);
?>

<!-- BEGIN PAGE CONTENT-->
<div class="row-fluid profile">
	<div class="span12">
		<!--BEGIN TABS-->
		<div class="tabbable tabbable-custom">
			<ul class="nav nav-tabs">
				<li class="active"><a href="#tab_1_1" data-toggle="tab">Descripción</a></li>
				<li><a href="#tab_1_2" data-toggle="tab">Fotos</a></li>
				<div class="actions offset9">
					<?php echo $this->Html->link('<i class="icon-edit"></i> Editar',
						array(
							'action' => 'edit',
							'controller' => 'rents',
							$rent['Rent']['id'],
							'admin' => true
							),
						array(
							'class' => 'btn grey',
							'escape' => false)
						);
					?>
					<?php
						echo $this->Form->postLink('<i class="icon-trash"></i> Eliminar',
							array('action' => 'delete', $rent['Rent']['id']),
							array('class'=>'btn red', 'escape'=>false),	__('Confirma que desea eliminar este inmueble en renta?'));
					?>
					<?php echo $this->Html->link('<i class="icon-tag"></i> Rentas',
						array(
							'action' => 'index',
							'controller' => 'rents',
							'admin' => true
							),
						array(
							'class' => 'btn purple',
							'escape' => false)
						);
					?>
				</div>
			</ul>
			<div class="tab-content">
				<div class="tab-pane row-fluid active" id="tab_1_1">
					<ul class="unstyled profile-nav span3">
						<li>
							<?php $img = isset($rent['Rent']['main_image']) && !empty($rent['Rent']['main_image']) ? '/img/images/rents_img_'.$rent['Rent']['main_image'] : 'http://www.placehold.it/820x674/EFEFEF/AAAAAA&amp;text=Sin+imagen+de+perfil'; ?>
							<?php echo $this->Html->image($img) ?>
						</li>
					</ul>
					<div class="span9">
						<div class="row-fluid">
							<div class="span12 profile-info">
								<h1><?php echo $rent['Rent']['type']; ?> en Renta</h1>
								<?php echo $rent['Rent']['description']; ?>
								<ul class="unstyled inline">
									<li><i class="icon-map-marker"></i> <?php echo $rent['City']['name'].', '.$rent['State']['name'] ?></li>
									<?php if($rent['Rent']['best_offer']): ?>
									<li><i class="icon-star"></i> Destacado</li>
									<?php endif; ?>
								</ul>
							</div>
							<!--end span12-->
						</div>
						<div class="profile-classic row-fluid">
							<ul class="unstyled span8">
								<li><span>Dirección:</span> <?php echo $rent['Rent']['address']; ?></li>
								<li><span>Tiempo de contrato:</span> <?php echo $rent['Rent']['contract_time']; ?></li>
								<li><span>Requisitos de renta:</span> <?php echo strip_tags($rent['Rent']['requirements']); ?></li>
							</ul>
						</div>
						<hr>
						<div class="row-fluid">
							<div class="span6">
								<div class="portlet sale-summary">
									<div class="portlet-title">
										<h4>Características</h4>
									</div>
									<ul class="unstyled">
										<li>
											<span class="sale-info">TIPO DE INMUEBLE</span>
											<span class="sale-num"><?php echo $rent['Rent']['type']; ?></span>
										</li>
										<li>
											<span class="sale-info">PLANTAS/PISOS</span>
											<span class="sale-num"><?php echo $rent['Rent']['floors']; ?></span>
										</li>
										<li>
											<span class="sale-info">RECÁMARAS/HABITACIONES</span>
											<span class="sale-num"><?php echo $rent['Rent']['bedrooms']; ?></span>
										</li>
										<li>
											<span class="sale-info">BAÑOS</span>
											<span class="sale-num"><?php echo $rent['Rent']['bathrooms']; ?></span>
										</li>
										<li>
											<span class="sale-info">DIMENSIÓN DEL TERRENO</span>
											<span class="sale-num"><?php echo $rent['Rent']['terrain_size']; ?></span>
										</li>
										<li>
											<span class="sale-info">COSTO DE RENTA</span>
											<span class="sale-num"><?php echo $this->Number->currency($rent['Rent']['cost'], 'USD'); ?></span>
										</li>
									</ul>
								</div>
							</div>
							<div class="span6">
								<?php if(!empty($rent['Rent']['latitude']) && !empty($rent['Rent']['longitude'])): ?>
								<!-- BEGIN GOOGLE MAP PORTLET-->
								<div class="portlet">
									<div class="portlet-title">
										<h4><i class="icon-globe"></i>Ubicación</h4>
									</div>
									<div class="portlet-body">
										<div class="label label-important visible-ie8">Not supported in Internet Explorer 8</div>
										<?php $mapImg = $this->Html->image('/img/images/rents_tbl_prev_'.$rent['Rent']['main_image']);?>
										<div id="gmap_marker" class="gmaps" data-latitude="<?php echo $rent['Rent']['latitude']; ?>" data-longitude="<?php echo $rent['Rent']['longitude']; ?>" data-image='<?php echo $mapImg; ?>'></div>
									</div>
								</div>
								<!-- END GOOGLE MAP PORTLET-->
								<?php endif; ?>
							</div>
						</div>
						<!--end row-fluid-->
					</div>
					<!--end span9-->
				</div>
				<!--end tab-pane-->
				<div class="tab-pane profile-classic row-fluid" id="tab_1_2">
					<?php
						echo $this->element(
							'admin/default_image_gallery',
							array(
								'data' => $rent['DefaultImage'],
								'imagePrefix' => 'rents',
								'modelId' => $rent['Rent']['id'],
								'portletTitle' => $rent['Rent']['type']=='Casa' ? 'Fotos de la Casa en Renta' : 'Fotos del departamento en renta',
								'model' => 'rent'
								)
							);
					?>
				</div>
				<!--end tab-pane-->
			</div>
		</div>
		<!--END TABS-->
	</div>
</div>
<!-- END PAGE CONTENT-->
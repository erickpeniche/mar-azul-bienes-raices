<?php
	echo $this->Html->css(
		array(
			'portal/jqtransform.css',
		),
		null,
		array('inline' => false, 'media'=>'screen')
	);

	echo $this->Html->script(array(
		'portal/jquery.jqtransform',
		'portal/jquery.validate.min',
		'portal/additional-methods.min',
		'portal/search-validation'
		), array('inline' => false)
	);
?>
<div class="wrapper">
	<div class="grid_3 alpha">
		<div class="indent-bottom11 p10-1">
			<h3 class="p3 word-sp">Buscador de propiedades</h3>
			<?php echo $this->element('portal/search-form', array('model' => 'Rent', 'controller' => 'rents')); ?>
		</div>
	</div>
	<div class="grid_9 omega">
		<div class="indent-bottom3 border-bottom">
			<h3 class="p3">Propiedades en Renta</h3>
			<?php echo $this->element('portal/properties_view', array('data' => $rents, 'prevPrefix' => 'rents_view_prev_', 'model' => 'Rent', 'folder'=>'rents')); ?>
		</div>
	</div>
</div>
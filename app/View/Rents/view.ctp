<?php
	echo $this->Html->css(
		array(
			'portal/prettyPhoto.css'
		),
		null,
		array('inline' => false, 'media'=>'screen')
	);

	echo $this->Html->script(array(
		'portal/jquery.prettyPhoto',
		'portal/hover-image',
		'https://maps.googleapis.com/maps/api/js?key=AIzaSyDFMUFMx5D3Cv41yLkhycfCzvxpi1i7B1Y&sensor=false',
		'portal/gmap-view'
		), array('inline' => false)
	);
?>
<div class="wrapper indent-bottom11 border-bottom p10-1">
	<div class="grid_8 alpha">
		<h3 class="p4"><?php echo $rent['Rent']['type'].' en renta'; ?></h3>
		<figure class="p5">
			<?php echo $this->Html->image('images/rents_view_img_'.$rent['Rent']['main_image']); ?>
		</figure>
		<?php echo $rent['Rent']['description']; ?>
	</div>
	<div class="grid_4 omega">
		<h3 class="p3">Ubicación</h3>
		<div class="wrapper indent-bottom5-1 border-bottom p5-1">
			<?php if(!empty($rent['Rent']['latitude']) && !empty($rent['Rent']['longitude'])): ?>
			<div id="map-canvas" data-lat="<?php echo $rent['Rent']['latitude']; ?>" data-lng="<?php echo $rent['Rent']['longitude']; ?>"></div><br>
			<?php endif; ?>
			<div class="extra-wrap">
				<ul class="list-1">
					<li><?php echo $rent['Rent']['address']; ?></li>
					<li class="last-item"><?php echo $rent['City']['name'].', '.$rent['State']['name']; ?></li>
				</ul>
			</div>
		</div>
		<h3 class="p3">Características</h3>
		<div class="wrapper indent-bottom5-1 border-bottom p5-1">
			<div class="grid_2 alpha">
				<ul class="list-1">
					<li><?php echo $rent['Rent']['floors']; ?> <?php echo $rent['Rent']['floors'] > 1 ? 'pisos' : 'piso'; ?></li>
					<li class="last-item"><?php echo $rent['Rent']['bedrooms']; ?> <?php echo $rent['Rent']['bedrooms'] > 1 ? 'habitaciones' : 'habitación'; ?></li>
				</ul>
			</div>
			<div class="grid_2 omega">
				<ul class="list-1">
					<li><?php echo $rent['Rent']['bathrooms']; ?> <?php echo $rent['Rent']['bathrooms'] > 1 ? 'baños' : 'baño'; ?></li>
					<li class="last-item"><?php echo $rent['Rent']['terrain_size']; ?></li>
				</ul>
			</div>
		</div>
		<h3 class="p3">Requerimientos</h3>
		<div class="wrapper indent-bottom5-1 border-bottom p5-1">
			<div class="extra-wrap">
				<ul class="list-1">
					<li>Tiempo de contrato: <?php echo $rent['Rent']['contract_time']; ?></li>
					<li class="last-item"><?php echo $rent['Rent']['requirements']; ?></li>
				</ul>
			</div>
		</div>
		<h3 class="p3">Costo de renta</h3>
		<div class="wrapper">
			<div class="extra-wrap">
				<ul class="list-1">
					<li class="last-item"><?php echo $this->Number->currency($rent['Rent']['cost'], 'USD'); ?></li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="wrapper">
	<div class="grid_12 alpha omega">
		<h3 class="p4">Galería de fotos</h3>
		<?php
			$totalimages = count($rent['DefaultImage']);
			$totalWrappers = round($totalimages / 3);

			$wrapper = 0;
			$boxClass = '';
			$boxes = 0;
		?>

		<!-- BEGIN TAB GRID CONTENT -->
		<?php if(!empty($rent['DefaultImage'])): ?>
			<?php foreach($rent['DefaultImage'] as $image): ?>
				<?php $boxes++ ?>
				<?php if ($boxes%3 == 1) : ?>
					<?php $wrapper++;?>
				<div class="wrapper <?php echo $wrapper!=$totalWrappers ? 'indent-bottom7-1' : ''; ?>">
				<?php endif ?>
					<?php
						switch ($boxes) {
							case 1:
								$boxClass = 'alpha';
								break;
							case 2:
								$boxClass = '';
								break;
							case 3:
								$boxClass = 'omega';
								break;
						}
					?>
					<div class="grid_4 <?php echo $boxClass; ?>">
						<figure class="p5">
							<?php
								$img = $this->Html->image('images/rentsthn/rentsthn_'.$image['file']);
								echo $this->Html->link($img, '/img/images/rents/rents_'.$image['file'], array('class'=>'lightbox-image', 'data-gal'=>'prettyPhoto[prettyPhoto]', 'escape'=>false))
							?>
						</figure>
					</div>
				<?php if ($boxes%3 == 0 || sizeof($rent['DefaultImage']) == $boxes) : ?>
					<?php $boxes = 0; ?>
				</div>
				<?php endif ?>
			<?php endforeach; ?>
		<?php else: ?>
		<h3 class="p5"><em>No hay imágenes para esta propiedad</em></h3>
		<?php endif; ?>
	</div>
</div>
<?php
	echo $this->Html->css(array(
		'admin/fancybox/source/jquery.fancybox',
		'admin/data-tables/DT_bootstrap',
		), null, array('inline' => false)
	);

	$this->set('activeMenu', 'sales');

	$this->Html->addCrumb('Ventas',
		array(
			'action' => 'index',
			'controller' => 'sales',
			'admin' => true
			)
		);
?>
<!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
	<div class="span12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet box light-grey">
			<div class="portlet-title">
				<h4><i class="icon-tag"></i>Ventas</h4>
				<div class="actions">
					<?php echo $this->Html->link('<i class="icon-plus"></i> Nueva</a>',
						array(
							'action' => 'add',
							'controller' => 'sales'
							),
						array(
							'class' => 'btn green',
							'escape' => false)
						);
					?>
				</div>
			</div>
			<div class="portlet-body">
				<table class="table table-striped table-bordered table-hover" id="sample_3">
					<thead>
						<tr>
							<th width='10%'>Foto</th>
							<th width='10%'><?php echo $this->Paginator->sort('State.name', 'Estado'); ?></th>
							<th width='10%'><?php echo $this->Paginator->sort('City.name', 'Ciudad'); ?></th>
							<th width='7%'><?php echo $this->Paginator->sort('type', 'Tipo'); ?></th>
							<th width='5%'><?php echo $this->Paginator->sort('floors', 'Plantas'); ?></th>
							<th width='9%'><?php echo $this->Paginator->sort('cost', 'Costo de Venta'); ?></th>
							<th width='6%'><?php echo $this->Paginator->sort('discount_amount', 'Descuento'); ?></th>
							<th width='5%'><?php echo $this->Paginator->sort('active', 'Estado'); ?></th>
							<th width='38%'class="actions" width=''><?php echo __('Acciones'); ?></th>
						</tr>
					</thead>
					<tbody>
						<?php if (empty($sales)) : ?>
						<tr>
							<td colspan="8">
								<div class="alert alert-block alert-info fade in">
									<button type="button" class="close" data-dismiss="alert"></button>
									<h4 class="alert-heading">No hay propiedades en venta!</h4>
									<p>
										Aún no se han agregado propiedades en venta. Haz click en el botón 'Nueva' para agregar una nueva propiedad en venta.
									</p>
								</div>
							</td>
						</tr>
						<?php else: ?>
						<?php foreach ($sales as $sale): ?>
						<?php $img = isset($sale['Sale']['main_image']) && !empty($sale['Sale']['main_image']) ? '/img/images/sales_tbl_prev_'.$sale['Sale']['main_image'] : 'http://www.placehold.it/100x100/EFEFEF/AAAAAA&amp;text=Sin+foto' ?>
						<tr class="odd gradeX">
							<td><?php echo $this->Html->link($this->Html->image($img), array('action'=>'view', $sale['Sale']['id'], 'admin'=>true), array('escape'=>false)); ?></td>
							<td><?php echo h($sale['State']['name']); ?>&nbsp;</td>
							<td><?php echo h($sale['City']['name']); ?>&nbsp;</td>
							<td><?php echo h($sale['Sale']['type']); ?>&nbsp;</td>
							<td>
								<?php
									echo $sale['Sale']['type'] != 'Terreno' ?  h($sale['Sale']['floors']) : '-';
								?>
							</td>
							<td><?php echo $this->Number->currency(h($sale['Sale']['cost']), 'USD'); ?>&nbsp;</td>
							<td><?php echo $sale['Sale']['discount_available'] ? h($sale['Sale']['discount_amount']).'%' : '-'; ?>&nbsp;</td>
							<td>
								<?php if($sale['Sale']['active']): ?>
								<span class="label label-important">En venta</span>
								<?php else: ?>
								<span class="label label-success">Vendida</span>
								<?php endif; ?>
							</td>
							<td class="actions">
								<?php
									$stripColor = $sale['Sale']['active'] ? 'grey' : 'green';
									echo $this->Html->link('<i class="icon-legal"></i> Vendida', array('action' => 'toggle_sold', $sale['Sale']['id'], 'admin'=>true), array('class'=>'btn mini '.$stripColor.'-stripe', 'escape'=>false));
								?>
								<?php
									$stripColor = $sale['Sale']['best_offer'] ? 'green' : 'grey';
									echo $this->Html->link('<i class="icon-certificate"></i> Oferta', array('action' => 'toggle_best_offer', $sale['Sale']['id'], 'admin'=>true), array('class'=>'btn mini '.$stripColor.'-stripe', 'escape'=>false));
								?>
								&nbsp;|&nbsp;
								<?php echo $this->Html->link('<i class="icon-info-sign"></i> Ver', array('action' => 'view', $sale['Sale']['id']), array('class'=>'btn mini blue-stripe', 'escape'=>false)); ?>
								<?php echo $this->Html->link('<i class="icon-edit"></i> Editar', array('action' => 'edit', $sale['Sale']['id']), array('class'=>'btn mini orange-stripe', 'escape'=>false)); ?>
								<?php echo $this->Form->postLink('<i class="icon-trash"></i> Eliminar', array('action' => 'delete', $sale['Sale']['id']), array('class'=>'btn mini red-stripe', 'escape'=>false), __('Confirma que desea eliminar esta propiedad en venta?')); ?>
							</td>
						</tr>
						<?php endforeach; ?>
						<?php endif; ?>
					</tbody>
				</table>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
<!-- END PAGE CONTENT-->

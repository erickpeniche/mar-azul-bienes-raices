<?php
	echo $this->Html->css(
		array(
			'admin/fancybox/source/jquery.fancybox',
			'admin/data-tables/DT_bootstrap'
			),
		null,
		array('inline' => false)
	);

	echo $this->Html->script(array('admin/jquery-slimscroll/jquery.slimscroll.min.js'), array('inline' => false));

	if(!empty($sale['Sale']['latitude']) && !empty($sale['Sale']['longitude'])){
		echo $this->Html->script(
			array(
				'http://maps.google.com/maps/api/js?sensor=true',
				'admin/js/gmaps.js',
				'admin/js/demo.gmaps.js'
			),
			array('inline' => false)
		);

		echo $this->Html->scriptBlock(
		    'jQuery(document).ready(function() {
				DemoGMaps.init();
			});',
		    array('inline' => false)
		);
	}

	$this->Html->addCrumb('Ventas',
		array(
			'action' => 'index',
			'controller' => 'sales',
			'admin' => true
			)
		);

	$this->set('activeMenu', 'sales');

	$this->Html->addCrumb('Ver', null);
?>

<!-- BEGIN PAGE CONTENT-->
<div class="row-fluid profile">
	<div class="span12">
		<!--BEGIN TABS-->
		<div class="tabbable tabbable-custom">
			<ul class="nav nav-tabs">
				<li class="active"><a href="#tab_1_1" data-toggle="tab">Descripción</a></li>
				<li><a href="#tab_1_2" data-toggle="tab">Fotos</a></li>
				<div class="actions offset9">
					<?php echo $this->Html->link('<i class="icon-edit"></i> Editar',
						array(
							'action' => 'edit',
							'controller' => 'sales',
							$sale['Sale']['id'],
							'admin' => true
							),
						array(
							'class' => 'btn grey',
							'escape' => false)
						);
					?>
					<?php
						echo $this->Form->postLink('<i class="icon-trash"></i> Eliminar',
							array('action' => 'delete', $sale['Sale']['id']),
							array('class'=>'btn red', 'escape'=>false),	__('Confirma que desea eliminar este inmueble en venta?'));
					?>
					<?php echo $this->Html->link('<i class="icon-money"></i> Ventas',
						array(
							'action' => 'index',
							'controller' => 'sales',
							'admin' => true
							),
						array(
							'class' => 'btn green',
							'escape' => false)
						);
					?>
				</div>
			</ul>
			<div class="tab-content">
				<div class="tab-pane row-fluid active" id="tab_1_1">
					<ul class="unstyled profile-nav span3">
						<li>
							<?php $img = isset($sale['Sale']['main_image']) && !empty($sale['Sale']['main_image']) ? '/img/images/sales_img_'.$sale['Sale']['main_image'] : 'http://www.placehold.it/820x674/EFEFEF/AAAAAA&amp;text=Sin+imagen+de+perfil'; ?>
							<?php echo $this->Html->image($img) ?>
						</li>
					</ul>
					<div class="span9">
						<div class="row-fluid">
							<div class="span12 profile-info">
								<h1><?php echo $sale['Sale']['type']; ?> en Venta</h1>
								<?php echo $sale['Sale']['description']; ?>
								<ul class="unstyled inline">
									<li><i class="icon-map-marker"></i> <?php echo $sale['City']['name'].', '.$sale['State']['name'] ?></li>
									<?php if($sale['Sale']['best_offer']): ?>
									<li><i class="icon-star"></i> Destacado</li>
									<?php endif; ?>
								</ul>
							</div>
							<!--end span12-->
						</div>
						<div class="profile-classic row-fluid">
							<ul class="unstyled span8">
								<li><span>Dirección:</span> <?php echo $sale['Sale']['address']; ?></li>
								<li><span>Créditos aceptados:</span> <?php echo $sale['Sale']['accepted_credits']; ?></li>
								<li><span>Requisitos de venta:</span> <?php echo strip_tags($sale['Sale']['requirements']); ?></li>
							</ul>
						</div>
						<hr>
						<div class="row-fluid">
							<div class="span6">
								<div class="portlet sale-summary">
									<div class="portlet-title">
										<h4>Características</h4>
									</div>
									<ul class="unstyled">
										<li>
											<span class="sale-info">TIPO DE INMUEBLE</span>
											<span class="sale-num"><?php echo $sale['Sale']['type']; ?></span>
										</li>
										<?php if($sale['Sale']['type']!='Terreno'): ?>
										<li>
											<span class="sale-info">PLANTAS/PISOS</span>
											<span class="sale-num"><?php echo $sale['Sale']['floors']; ?></span>
										</li>
										<li>
											<span class="sale-info">RECÁMARAS/HABITACIONES</span>
											<span class="sale-num"><?php echo $sale['Sale']['bedrooms']; ?></span>
										</li>
										<li>
											<span class="sale-info">BAÑOS</span>
											<span class="sale-num"><?php echo $sale['Sale']['bathrooms']; ?></span>
										</li>
										<?php endif; ?>
										<li>
											<span class="sale-info">DIMENSIÓN DEL TERRENO</span>
											<span class="sale-num"><?php echo $sale['Sale']['terrain_size']; ?></span>
										</li>
										<li>
											<span class="sale-info">COSTO DE VENTA</span>
											<span class="sale-num"><?php echo $this->Number->currency($sale['Sale']['cost'], 'USD'); ?></span>
										</li>
										<li>
											<span class="sale-info">DESCUENTO DISPONIBLE</span>
											<span class="sale-num"><?php echo $sale['Sale']['discount_available'] ? 'Sí' : 'No'; ?></span>
										</li>
										<?php if($sale['Sale']['discount_available']): ?>
										<li>
											<span class="sale-info">CANTIDAD DE DESCUENTO</span>
											<span class="sale-num"><?php echo $sale['Sale']['discount_amount'].'%'; ?></span>
										</li>
										<?php endif; ?>
									</ul>
								</div>
							</div>
							<div class="span6">
								<?php if(!empty($sale['Sale']['latitude']) && !empty($sale['Sale']['longitude'])): ?>
								<!-- BEGIN GOOGLE MAP PORTLET-->
								<div class="portlet">
									<div class="portlet-title">
										<h4><i class="icon-globe"></i>Ubicación</h4>
									</div>
									<div class="portlet-body">
										<div class="label label-important visible-ie8">Not supported in Internet Explorer 8</div>
										<?php $mapImg = $this->Html->image('/img/images/sales_tbl_prev_'.$sale['Sale']['main_image']);?>
										<div id="gmap_marker" class="gmaps" data-latitude="<?php echo $sale['Sale']['latitude']; ?>" data-longitude="<?php echo $sale['Sale']['longitude']; ?>" data-image='<?php echo $mapImg; ?>'></div>
									</div>
								</div>
								<!-- END GOOGLE MAP PORTLET-->
								<?php endif; ?>
							</div>
						</div>
						<!--end row-fluid-->
					</div>
					<!--end span9-->
				</div>
				<!--end tab-pane-->
				<div class="tab-pane profile-classic row-fluid" id="tab_1_2">
					<?php
						switch ($sale['Sale']['type']) {
							case 'Casa':
								$saleGalleryText = 'Fotos de la Casa en Venta';
								break;
							case 'Departamento':
								$saleGalleryText = 'Fotos del Departamento en Venta';
								break;
							case 'Terreno':
								$saleGalleryText = 'Fotos del Terreno en Venta';
								break;
						}
						echo $this->element(
							'admin/default_image_gallery',
							array(
								'data' => $sale['DefaultImage'],
								'imagePrefix' => 'sales',
								'modelId' => $sale['Sale']['id'],
								'portletTitle' => $saleGalleryText,
								'model' => 'sale'
								)
							);
					?>
				</div>
				<!--end tab-pane-->
			</div>
		</div>
		<!--END TABS-->
	</div>
</div>
<!-- END PAGE CONTENT-->
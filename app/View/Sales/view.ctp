<?php
	echo $this->Html->css(
		array(
			'portal/prettyPhoto.css'
		),
		null,
		array('inline' => false, 'media'=>'screen')
	);

	echo $this->Html->script(array(
		'portal/jquery.prettyPhoto',
		'portal/hover-image',
		'https://maps.googleapis.com/maps/api/js?key=AIzaSyDFMUFMx5D3Cv41yLkhycfCzvxpi1i7B1Y&sensor=false',
		'portal/gmap-view'
		), array('inline' => false)
	);
?>
<div class="wrapper indent-bottom11 border-bottom p10-1">
	<div class="grid_8 alpha">
		<h3 class="p4"><?php echo $sale['Sale']['type'].' en venta'; ?></h3>
		<figure class="p5">
			<?php echo $this->Html->image('images/sales_view_img_'.$sale['Sale']['main_image']); ?>
		</figure>
		<?php echo $sale['Sale']['description']; ?>
	</div>
	<div class="grid_4 omega">
		<h3 class="p3">Ubicación</h3>
		<div class="wrapper indent-bottom5-1 border-bottom p5-1">
			<?php if(!empty($sale['Sale']['latitude']) && !empty($sale['Sale']['longitude'])): ?>
			<div id="map-canvas" data-lat="<?php echo $sale['Sale']['latitude']; ?>" data-lng="<?php echo $sale['Sale']['longitude']; ?>"></div><br>
			<?php endif; ?>
			<div class="extra-wrap">
				<ul class="list-1">
					<li><?php echo $sale['Sale']['address']; ?></li>
					<li class="last-item"><?php echo $sale['City']['name'].', '.$sale['State']['name']; ?></li>
				</ul>
			</div>
		</div>
		<h3 class="p3">Características</h3>
		<div class="wrapper indent-bottom5-1 border-bottom p5-1">
			<div class="grid_2 alpha">
				<ul class="list-1">
					<li><?php echo $sale['Sale']['floors']; ?> <?php echo $sale['Sale']['floors'] > 1 ? 'pisos' : 'piso'; ?></li>
					<li class="last-item"><?php echo $sale['Sale']['bedrooms']; ?> <?php echo $sale['Sale']['bedrooms'] > 1 ? 'habitaciones' : 'habitación'; ?></li>
				</ul>
			</div>
			<div class="grid_2 omega">
				<ul class="list-1">
					<li><?php echo $sale['Sale']['bathrooms']; ?> <?php echo $sale['Sale']['bathrooms'] > 1 ? 'baños' : 'baño'; ?></li>
					<li class="last-item"><?php echo $sale['Sale']['terrain_size']; ?></li>
				</ul>
			</div>
		</div>
		<h3 class="p3">Requerimientos</h3>
		<div class="wrapper indent-bottom5-1 border-bottom p5-1">
			<div class="extra-wrap">
				<ul class="list-1">
					<li class="last-item"><?php echo $sale['Sale']['requirements']; ?></li>
				</ul>
			</div>
		</div>
		<h3 class="p3">Créditos aceptados</h3>
		<?php $credits = explode(', ', $sale['Sale']['accepted_credits']); ?>
		<?php if(count($credits) > 1): ?>
		<div class="wrapper indent-bottom5-1 border-bottom p5-1">
			<?php $half = round(count($credits)/2); ?>
			<div class="grid_2 alpha">
				<ul class="list-1">
					<?php for($i=0; $i<$half; $i++): ?>
					<li class="<?php echo $i==($half-1) ? 'last-item':''; ?>"><?php echo $credits[$i]; ?></li>
					<?php endfor; ?>
				</ul>
			</div>
			<div class="grid_2 omega">
				<ul class="list-1">
					<?php for($i=$half; $i<count($credits); $i++): ?>
					<li class="<?php echo $i==(count($credits)-1) ? 'last-item':''; ?>"><?php echo $credits[$i]; ?></li>
					<?php endfor; ?>
				</ul>
			</div>
		</div>
		<?php else: ?>
		<div class="wrapper indent-bottom5-1 border-bottom p5-1">
			<div class="extra-wrap">
				<ul class="list-1">
					<li class="last-item"><?php echo $credits[0]; ?></li>
				</ul>
			</div>
		</div>
		<?php endif; ?>
		<h3 class="p3">Costo de venta</h3>
		<div class="wrapper">
			<div class="extra-wrap">
				<ul class="list-1">
					<li class="<?php echo $sale['Sale']['discount_available'] ? '':'last-item'; ?>"><?php echo $this->Number->currency($sale['Sale']['cost'], 'USD'); ?></li>
					<?php if($sale['Sale']['discount_available']): ?>
					<li class="last-item"><?php echo 'Descuento del '.$sale['Sale']['discount_amount'].'%'; ?></li>
					<?php endif; ?>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="wrapper">
	<div class="grid_12 alpha omega">
		<h3 class="p4">Galería de fotos</h3>
		<?php
			$totalimages = count($sale['DefaultImage']);
			$totalWrappers = round($totalimages / 3);

			$wrapper = 0;
			$boxClass = '';
			$boxes = 0;
		?>

		<!-- BEGIN TAB GRID CONTENT -->
		<?php if(!empty($sale['DefaultImage'])): ?>
			<?php foreach($sale['DefaultImage'] as $image): ?>
				<?php $boxes++ ?>
				<?php if ($boxes%3 == 1) : ?>
					<?php $wrapper++;?>
				<div class="wrapper <?php echo $wrapper!=$totalWrappers ? 'indent-bottom7-1' : ''; ?>">
				<?php endif ?>
					<?php
						switch ($boxes) {
							case 1:
								$boxClass = 'alpha';
								break;
							case 2:
								$boxClass = '';
								break;
							case 3:
								$boxClass = 'omega';
								break;
						}
					?>
					<div class="grid_4 <?php echo $boxClass; ?>">
						<figure class="p5">
							<?php
								$img = $this->Html->image('images/salesthn/salesthn_'.$image['file']);
								echo $this->Html->link($img, '/img/images/sales/sales_'.$image['file'], array('class'=>'lightbox-image', 'data-gal'=>'prettyPhoto[prettyPhoto]', 'escape'=>false))
							?>
						</figure>
					</div>
				<?php if ($boxes%3 == 0 || sizeof($sale['DefaultImage']) == $boxes) : ?>
					<?php $boxes = 0; ?>
				</div>
				<?php endif ?>
			<?php endforeach; ?>
		<?php else: ?>
		<h3 class="p5"><em>No hay imágenes para esta propiedad</em></h3>
		<?php endif; ?>
	</div>
</div>
<?php
	echo $this->Html->css(array(
		'admin/fancybox/source/jquery.fancybox',
		'admin/data-tables/DT_bootstrap',
		), null, array('inline' => false)
	);

	$this->set('activeMenu', 'subscribers');

	$this->Html->addCrumb('Suscriptores y boletín',
		array(
			'action' => 'index',
			'controller' => 'subscribers',
			'admin' => true
			)
		);
?>
<!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
	<div class="span12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet box light-grey">
			<div class="portlet-title">
				<h4><i class="icon-envelope"></i>Suscriptores</h4>
				<div class="actions">
					<?php echo $this->Html->link('<i class="icon-envelope"></i> Enviar boletín</a>',
						array(
							'action' => 'send_newsletter',
							'controller' => 'subscribers'
							),
						array(
							'class' => 'btn green',
							'escape' => false)
						);
					?>
				</div>
			</div>
			<div class="portlet-body">
				<table class="table table-striped table-bordered table-hover" id="sample_3">
					<thead>
						<tr>
							<th><?php echo $this->Paginator->sort('email', 'E-mail'); ?></th>
							<th><?php echo $this->Paginator->sort('active', 'Estado'); ?></th>
							<th><?php echo $this->Paginator->sort('created', 'Creado'); ?></th>
							<th><?php echo $this->Paginator->sort('modified', 'Modificado'); ?></th>
							<th class="actions"><?php echo __('Acciones'); ?></th>
						</tr>
					</thead>
					<tbody>
					<?php if (empty($subscribers)) : ?>
						<tr>
							<td colspan="5">
								<div class="alert alert-block alert-info fade in">
									<button type="button" class="close" data-dismiss="alert"></button>
									<h4 class="alert-heading">No hay suscriptores en la lista!</h4>
								</div>
							</td>
						</tr>
					<?php else: ?>
						<?php foreach ($subscribers as $subscriber): ?>
						<tr class="odd gradeX">
							<td><?php echo h($subscriber['Subscriber']['email']); ?>&nbsp;</td>
							<td>
								<?php
									if($subscriber['Subscriber']['active']==1){
										echo '<span class="label label-success">Suscrito</span>';
									}else{
										echo '<span class="label label-important">Sin suscripción</span>';
									}
								?>
							</td>
							<td><?php echo h($subscriber['Subscriber']['created']); ?>&nbsp;</td>
							<td><?php echo h($subscriber['Subscriber']['modified']); ?>&nbsp;</td>
							<td class="actions">
								<?php
									$stripColor = $subscriber['Subscriber']['active'] ? 'green' : 'grey';
									$icon = $subscriber['Subscriber']['active'] ? 'check' : 'check-empty';
									echo $this->Html->link('<i class="icon-'.$icon.'"></i> Suscripción', array('action' => 'toggle_active', $subscriber['Subscriber']['id'], 'admin'=>true), array('class'=>'btn mini '.$stripColor.'-stripe', 'escape'=>false));
								?>
								&nbsp;|&nbsp;
								<?php echo $this->Form->postLink('<i class="icon-trash"></i> Eliminar', array('action' => 'delete', $subscriber['Subscriber']['id']), array('class'=>'btn mini red-stripe', 'escape'=>false), __('Confirma que desea dar de baja está dirección del servicio de suscripción?')); ?>
							</td>
						</tr>
						<?php endforeach; ?>
					<?php endif; ?>
					</tbody>
				</table>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
<!-- END PAGE CONTENT-->

<?php
	$img = isset($user['User']['image']) && !empty($user['User']['image']) ? '/img/images/user_img_'.$user['User']['image'] : 'http://www.placehold.it/820x674/EFEFEF/AAAAAA&amp;text=Sin+imagen+de+perfil';
	$this->Html->addCrumb('Usuarios',
		array(
			'action' => 'index',
			'controller' => 'users',
			'admin' => true
			)
		);

	$this->set('activeMenu', 'users');

	$this->Html->addCrumb('Ver', null);

	$this->Html->addCrumb($user['User']['first_name']. ' ' . $user['User']['last_name'],
		array(
			'action' => 'view',
			'controller' => 'users',
			$user['User']['id'],
			Format::clean($user['User']['first_name']. ' ' .$user['User']['last_name']),
			'admin' => true
			)
		);
?>
<div class="row-fluid">
	<div class="span3 offset9">
		<div class="actions">
			<?php echo $this->Html->link('<i class="icon-edit"></i> Editar',
				array(
					'action' => 'edit',
					'controller' => 'users',
					$user['User']['id'],
					Format::clean($user['User']['first_name']. ' ' .$user['User']['last_name']),
					'admin' => true
					),
				array(
					'class' => 'btn grey',
					'escape' => false)
				);
			?>
			<?php
				echo $isSuperUser && $loggedUser['id'] != $user['User']['id'] ? $this->Form->postLink('<i class="icon-trash"></i> Eliminar',
					array('action' => 'delete', $user['User']['id']),
					array('class'=>'btn red', 'escape'=>false),
					__('Confirma que desea eliminar a %s?', $user['User']['first_name']. ' ' .$user['User']['last_name'])) : '';
			?>
			<?php echo $isSuperUser ? $this->Html->link('<i class="icon-user"></i> Usuarios',
				array(
					'action' => 'index',
					'controller' => 'users',
					'admin' => true
					),
				array(
					'class' => 'btn blue',
					'escape' => false)
				) : '';
			?>
		</div>
	</div>
</div>
<div class="profile-classic row-fluid">
	<div class="span2">
		<img src="<?php echo $img; ?>" alt="" />
	</div>
	<ul class="unstyled span10">
		<li><span>Nombre: </span><?php echo $user['User']['first_name'] ?></li>
		<li><span>Apellido: </span> <?php echo $user['User']['last_name'] ?></li>
		<li><span>E-mail: </span> <?php echo $user['User']['email'] ?></li>
		<li><span>Rol: </span> <?php echo $user['User']['role'] ?></li>
		<li><span>Activo: </span> <?php echo $user['User']['active'] ? 'Sí' : 'No'; ?></li>
		<li><span>Creado: </span> <?php echo $user['User']['created'] ?></li>
		<li><span>Modificado: </span> <?php echo $user['User']['modified'] ?></li>
	</ul>
</div>
var s,
adminEdit = {
	settings: {
		fields: {
			location: null
		},
		coordinates: null,
		zoom: 0,
		map: null
	},
	init: function() {
		s = this.settings;
		var th = this;
		s.fields.location = document.getElementById('ConfigurationValue');

		if (s.fields.location.value) {
			s.coordinates = s.fields.location.value.split(',');
			s.zoom = 15;
		} else {
			s.coordinates = [19.47695, -99.118652];
			s.zoom = 4;
		}
		s.map = new GMaps({
			div: '#gmap_marker',
			lat: s.coordinates[0],
			lng: s.coordinates[1],
			zoom: s.zoom,
			click: th.onMapClick,
		});
		s.map.addMarker({
			lat: s.coordinates[0],
			lng: s.coordinates[1],
			draggable: true,
			dragend: th.onDragEnd
		});

		$('button#clean-map').bind('click', this.onClean);
	},

	onMapClick: function(e) {
		s.fields.location.value = e.latLng.nb+','+e.latLng.ob;
		var latLng = new google.maps.LatLng(e.latLng.nb, e.latLng.ob);
		s.map.markers[0].setPosition(latLng);
		s.map.setCenter(e.latLng.nb, e.latLng.ob);
	},

	onDragEnd: function(e) {
		s.fields.location.value = e.latLng.nb+','+e.latLng.ob;
		s.map.setCenter(e.latLng.nb, e.latLng.ob);
	},

	onClean: function() {
		s.fields.location.value = '';
		var latLng = new google.maps.LatLng(19.47695, -99.118652);
		s.map.markers[0].setPosition(latLng);
		s.map.setCenter(19.47695, -99.118652);
		s.map.setZoom(4);
	}
}

$(document).ready(function () {
	adminEdit.init();
});
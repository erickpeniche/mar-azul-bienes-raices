var discount = {
	init: function() {
		$('.discount').change(function() {
			$('#discountAmount').slideToggle();
		});

		$('#SaleType').change(function(){
			if($(this).val()==='Terreno'){
				$('#floors, #bedrooms, #bathrooms').slideUp();
			} else{
				$('#floors, #bedrooms, #bathrooms').slideDown();
			}
		});
	}
}

$(document).ready(function () {
	discount.init();
});
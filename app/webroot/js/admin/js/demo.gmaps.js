var DemoGMaps = function () {
		var mapMarker = function () {
			var map = new GMaps({
					div: '#gmap_marker',
					lat: $('#gmap_marker').data('latitude'),
					lng: $('#gmap_marker').data('longitude')
			});
			map.addMarker({
					lat: $('#gmap_marker').data('latitude'),
					lng: $('#gmap_marker').data('longitude'),
					title: 'Marker with InfoWindow',
					infoWindow: {
							content: $('#gmap_marker').data('image')
					}
			});
		}

		return {
				//main function to initiate map samples
				init: function () {
						mapMarker();
				}

		};

}();
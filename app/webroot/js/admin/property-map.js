var s,
pMap = {
	settings: {
		fields: {
			lat: null,
			lng: null,
		},
		coordinates: {
			lat: null,
			lng: null,
		},
		zoom: 0,
		map: null,
		model: null
	},
	init: function() {
		s = this.settings;
		var th = this;
		s.model = $('div#gmap_marker').data('model');

		s.fields.lat = document.getElementById(s.model+'Latitude');
		s.fields.lng = document.getElementById(s.model+'Longitude');

		if (s.fields.lat.value && s.fields.lng.value) {
			s.coordinates.lat = s.fields.lat.value;
			s.coordinates.lng = s.fields.lng.value;
			s.zoom = 15;
		} else {
			s.coordinates.lat = 19.47695;
			s.coordinates.lng = -99.118652;
			s.zoom = 4;
		}
		s.map = new GMaps({
			div: '#gmap_marker',
			lat: s.coordinates.lat,
			lng: s.coordinates.lng,
			zoom: s.zoom,
			click: th.onMapClick,
		});
		s.map.addMarker({
			lat: s.coordinates.lat,
			lng: s.coordinates.lng,
			draggable: true,
			dragend: th.onDragEnd
		});

		$('button#clean-map').bind('click', this.onClean);
	},

	onMapClick: function(e) {
		s.fields.lat.value = e.latLng.nb;
		s.fields.lng.value = e.latLng.ob;
		var latLng = new google.maps.LatLng(e.latLng.nb, e.latLng.ob);
		s.map.markers[0].setPosition(latLng);
		s.map.setCenter(e.latLng.nb, e.latLng.ob);
	},

	onDragEnd: function(e) {
		s.fields.lat.value = e.latLng.nb;
		s.fields.lng.value = e.latLng.ob;
		s.map.setCenter(e.latLng.nb, e.latLng.ob);
	},

	onClean: function() {
		s.fields.lat.value = '';
		s.fields.lng.value = '';
		var latLng = new google.maps.LatLng(19.47695, -99.118652);
		s.map.markers[0].setPosition(latLng);
		s.map.setCenter(19.47695, -99.118652);
		s.map.setZoom(4);
	}
}

$(document).ready(function () {
	pMap.init();
});
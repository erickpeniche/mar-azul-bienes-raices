var adminAddRent = {
	init: function() {
		if($('#RentStateId').val()){
			if($('#city-dropdown').data('city')){
				var cityId = $('#city-dropdown').data('city');
				$('#city-dropdown').load('/cities/getCities/Rent/'+$('#RentStateId').val()+'/'+cityId);
			} else{
				$('#city-dropdown').load('/cities/getCities/Rent/'+$('#RentStateId').val());
			}
		}
		$('#RentStateId').change(function() {
			$('#city-dropdown').load('/cities/getCities/Rent/'+$(this).val());
		});
	}
}

$(document).ready(function () {
	adminAddRent.init();
});
var adminAddSale = {
	init: function() {
		if($('#SaleStateId').val()){
			if($('#city-dropdown').data('city')){
				var cityId = $('#city-dropdown').data('city');
				$('#city-dropdown').load('/cities/getCities/Sale/'+$('#SaleStateId').val()+'/'+cityId);
			} else{
				$('#city-dropdown').load('/cities/getCities/Sale/'+$('#SaleStateId').val());
			}
		}
		$('#SaleStateId').change(function() {
			$('#city-dropdown').load('/cities/getCities/Sale/'+$(this).val());
		});
	}
}

$(document).ready(function () {
	adminAddSale.init();
});
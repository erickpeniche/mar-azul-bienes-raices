$(window).load(function(){
	$('#contact-form').forms({
		mailHandlerURL:'contacto',
		controls:'a[data-type=reset],a[data-form=contact]'
	})

	$('div#gmap').height('271px');
	var data = $('div#gmap').data('coordinates');
	var coordinates = data.split(',');

	google.maps.visualRefresh = true;
	var latlng = new google.maps.LatLng(coordinates[0], coordinates[1]);

	var mapOptions = {
		center: latlng,
		zoom: 14,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		keyboardShortcuts: false,
	};

	var map = new google.maps.Map(document.getElementById("gmap"), mapOptions);

	var marker = new google.maps.Marker({
		position: latlng,
		map: map
	});
})
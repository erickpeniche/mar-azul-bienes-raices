var s,
mapView = {
	settings: {
		lat: null,
		lng: null,
		map: null
	},
	init: function() {
		s = this.settings;
		var th = this;
		if ($('#map-canvas').length) {
			$('#map-canvas').height('214px');
			s.lat = $('#map-canvas').data('lat');
			s.lng = $('#map-canvas').data('lng');

			google.maps.visualRefresh = true;
			var latlng = new google.maps.LatLng(s.lat, s.lng);

			var mapOptions = {
				center: latlng,
				zoom: 14,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			};

			s.map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);

			var marker = new google.maps.Marker({
				position: latlng,
				map: s.map
			});
		};
	}
}

$(document).ready(function () {
	mapView.init();
});
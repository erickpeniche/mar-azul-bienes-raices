$(document).ready(function () {
	var searchForm = $("#full-search-form");
	searchForm.validate({
		submitHandler: function(form) {
			form.submit();
		},

	});

	jQuery.validator.addMethod("maxPrice", function(value, element, params) {
		if($(params).val()){
			var maxPriceVal = parseInt($(params).val());
			if (value<maxPriceVal) {return true;} else{return false;};
		}
		return true;
	}, jQuery.validator.format("*Cantidad inválida"));

	jQuery.validator.addMethod("minPrice", function(value, element, params) {
		if($(params).val()){
			var minPriceVal = parseInt($(params).val());
			if (value>minPriceVal) {return true;} else{return false;};
		}
		return true;
	}, jQuery.validator.format("*Cantidad inválida"));

	$('.maxPriceCustom').rules("add", {
		required: '.minPriceCustom:filled',
		minPrice: '.minPriceCustom',
		number: true,
		messages: {
			number: jQuery.format("*Cantidad inválida"),
			required: jQuery.format("*Campo requerido")
		}
	});

	$('.minPriceCustom').rules("add", {
		required: '.maxPriceCustom:filled',
		maxPrice: '.maxPriceCustom',
		number: true,
		messages: {
			number: jQuery.format("*Cantidad inválida"),
			required: jQuery.format("*Campo requerido")
		}
	});

	$('.stateCity').rules("add", {
		lettersonly: true,
		messages: {
			lettersonly: jQuery.format("*Ciudad o estado inválido")
		}
	});

	$('a.button').first().bind( "click", function(e) {
		searchForm.submit();
	});
});
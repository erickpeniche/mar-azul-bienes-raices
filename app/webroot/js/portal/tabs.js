$(function(){
	tabs.init();
});
	
tabs = {
	init : function(){
		$('ul.nav li.first-item').addClass('selected');
		$('.tabs').each(function(){
			$(this).find('.tab_content:gt(0)').hide();
			$(this).find('ul.nav a').click(function(){
				$(this).parents('div.tabs').find('.tab_content').hide();
				$($(this).attr('href')).show();
				$(this).parent().addClass('selected').siblings().removeClass('selected');
				return false;
			});
		});
	}
}